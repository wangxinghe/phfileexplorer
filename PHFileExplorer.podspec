
Pod::Spec.new do |s|
  s.name         = "PHFileExplorer"
  s.version      = "0.1.6"
  s.summary      = "A Generic Framework for Directory and File Explorer!"
  s.homepage     = "https://bitbucket.org"
  s.frameworks   = 'UIKit', 'Foundation', 'CoreGraphics'
  s.license      = {:type => 'Philology',
  					:text => <<-LICENSE 
  								Copyright (C) 2013 Philology Pty. 
  								Ltd.All rights reserved.
  								LICENSE
  					}
  s.author       = { "Ricol Wang" => "ricol.wang@philology.com.au" }
  s.source       = { :git => "git@bitbucket.org:philology/PHFileExplorer.git", :tag => s.version.to_s }
  s.platform     = :ios, '6.1'
  s.source_files = 'PHFileExplorer/Classes/**/*.{h,m}'
  s.resources = 'PHFileExplorer/Resources/**/*.{png}', 'PHFileExplorer/Classes/**/*.{xib}', 'PHFileExplorer/*.lproj'
  s.exclude_files = 'PHFileExplorer/AppDelegate.{h,m}', 'PHFileExplorer/main.{m}'
  s.requires_arc = true

  s.dependency 'DirectoryWatchdog', '1.1.0'
  s.dependency 'GMGridView', '1.1.1'
  s.dependency 'ZipArchive', '1.3.0'
  s.dependency 'PHCommon', '0.2.9'
  s.dependency 'GVPhotoBrowser', '0.2.3'
  s.dependency 'MBProgressHUD', '0.9'
end
