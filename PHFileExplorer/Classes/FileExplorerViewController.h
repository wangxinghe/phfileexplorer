//
//  FileExplorerViewController.h
//  PHFileExplorer
//
//  Created by Ricol Wang on 7/08/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import "BasicFileExplorerViewController.h"

@interface FileExplorerViewController : BasicFileExplorerViewController

- (id)initWithPath:(NSString *)path andRoot:(NSString *)root andRowForIpadLandscape:(int)rowForIPadLandscape andRowForIPadPortrait:(int)rowForIPadPortrait andRowForIPhoneLandscape:(int)rowForIPhoneLandscape andRowForIPhonePortrait:(int)rowForIPhonePortrait;

@end
