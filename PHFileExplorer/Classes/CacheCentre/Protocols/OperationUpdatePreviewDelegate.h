//
//  OperationUpdatePreviewDelegate.h
//  PHFileExplorer
//
//  Created by Ricol Wang on 8/07/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol OperationUpdatePreviewDelegate <NSObject>

- (void)operationUpdatePreviewComleteWith:(UIImage *)preview andKey:(NSString *)key;

@end
