//
//  IUpdateGUIWithPreview.h
//  PHFileExplorer
//
//  Created by Ricol Wang on 8/07/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IUpdateGUIWithPreview <NSObject>

- (void)updateGuiWithPreview:(UIImage *)preview withFilepath:(NSString *)filepath;
- (CGSize)getPreviewSize;

@end
