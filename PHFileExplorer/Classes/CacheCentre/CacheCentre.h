//
//  CacheCentre.h
//  PHFileExplorer
//
//  Created by Ricol Wang on 7/07/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IUpdateGUIWithPreview.h"

@interface CacheCentre : NSObject

+ (instancetype)sharedInstance;

- (void)requestPreviewFilePath:(NSString *)filepath andInterface:(id <IUpdateGUIWithPreview>)theInterface withDefaultImage:(UIImage *)image;
- (void)setMaximumCurrentExecutionThreads:(int)number;
- (void)saveToDisk:(BOOL)bDisk;
- (void)setPreviewDirectory:(NSString *)directory;

@end
