//
//  CacheCentre.m
//  PHFileExplorer
//
//  Created by Ricol Wang on 7/07/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import "CacheCentre.h"
#import "OperationUpdatePreview.h"
#import "OperationUpdatePreviewDelegate.h"
#import <PHCommon/utils.h>
#import <CommonCrypto/CommonHMAC.h>

#define DEFAULT_CONCURRENT_EXECUTION_THREADS 5

@interface CacheCentre () <OperationUpdatePreviewDelegate>

@property BOOL bSaveDisk;
@property (copy) NSString *thePreviewDirectory;
@property (strong) NSFileManager *fm;
@property (strong) NSMutableDictionary *mapPreview;
@property (strong) NSOperationQueue *operationQueue;

@end

@implementation CacheCentre

- (instancetype)init
{
    self = [super init];
    
    if (self)
    {
        self.fm = [NSFileManager defaultManager];
        self.mapPreview = [NSMutableDictionary new];
        self.operationQueue = [NSOperationQueue new];
        self.operationQueue.maxConcurrentOperationCount = DEFAULT_CONCURRENT_EXECUTION_THREADS;
        self.thePreviewDirectory = @".Previews";
    }
    
    return self;
}

#pragma mark - OperationUpdatePreviewDelegate

- (void)operationUpdatePreviewComleteWith:(UIImage *)preview andKey:(NSString *)key
{
    if (preview)
        [self updatePreviewForKey:key andPreview:preview];
}

#pragma mark - Public Static Methods

+ (instancetype)sharedInstance
{
    static CacheCentre *instance = nil;
    if (!instance)
        instance = [CacheCentre new];
    return instance;
}

+ (NSString *)getMD5FromString:(NSString *)source
{
    const char *src = [source UTF8String];
    unsigned char result[16];
    CC_MD5(src, strlen(src), result);
    NSString *ret = [[NSString alloc] initWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                     result[0], result[1], result[2], result[3],
                     result[4], result[5], result[6], result[7],
                     result[8], result[9], result[10], result[11],
                     result[12], result[13], result[14], result[15]
                     ];
    return [ret lowercaseString];
}

#pragma mark - Public Methods

- (void)requestPreviewFilePath:(NSString *)filepath andInterface:(id <IUpdateGUIWithPreview>)theInterface withDefaultImage:(UIImage *)image
{
    NSError *error = nil;
    NSDictionary *dict = [self.fm attributesOfItemAtPath:filepath error:&error];
    NSDate *date = [dict valueForKey:NSFileModificationDate];
    float timestamp = date.timeIntervalSince1970;
    NSString *strKey = [NSString stringWithFormat:@"%@(%f)", filepath, timestamp];
        NSData *data = [strKey dataUsingEncoding:NSUTF8StringEncoding];
        strKey = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            strKey = [CacheCentre getMD5FromString:strKey];
    
    UIImage *preview = [self getPreviewForKey:strKey];
    if (preview)
    {
        [theInterface updateGuiWithPreview:preview withFilepath:filepath];
    }else
    {
        [theInterface updateGuiWithPreview:image withFilepath:filepath];
        
        CGSize size = [theInterface getPreviewSize];
        
        OperationUpdatePreview *preview = [[OperationUpdatePreview alloc] initWithFilePath:filepath andTimestamp:timestamp andKey:strKey andSize:size andDelegate:self andInterface:theInterface];
        [self.operationQueue addOperation:preview];
    }
}

- (void)setMaximumCurrentExecutionThreads:(int)number
{
    if (number > 0 && number <= 10)
        self.operationQueue.maxConcurrentOperationCount = number;
}

- (void)saveToDisk:(BOOL)bDisk
{
    self.bSaveDisk = bDisk;
}

- (void)setPreviewDirectory:(NSString *)directory
{
    if (directory && ![directory isEqualToString:@""])
    {
        self.previewDirectory = directory;
    }
}

#pragma mark - Private Methods

- (UIImage *)getPreviewForKey:(NSString *)key
{
    if (key)
    {
        if (self.bSaveDisk)
        {
            NSString *file = [NSString stringWithFormat:@"%@/%@/%@.jpg", DOCUMENTS_DIRECTORY, self.thePreviewDirectory, key];
            BOOL bDirectory = NO;
            if ([self.fm fileExistsAtPath:file isDirectory:&bDirectory])
            {
                if (!bDirectory)
                    return [UIImage imageWithContentsOfFile:file];
                else
                    return nil;
            }
        }else
        {
            return self.mapPreview[key];
        }
    }
    
    return nil;
}

- (void)updatePreviewForKey:(NSString *)key andPreview:(UIImage *)preview
{
    if (preview && key)
    {
        if (self.bSaveDisk)
        {
            NSString *fileDirectory = [NSString stringWithFormat:@"%@/%@", DOCUMENTS_DIRECTORY, self.thePreviewDirectory];
            BOOL bDirectory = NO;
            if (![self.fm fileExistsAtPath:fileDirectory isDirectory:&bDirectory])
            {
                NSError *error = nil;
                [self.fm createDirectoryAtPath:fileDirectory withIntermediateDirectories:YES attributes:nil error:&error];
            }
            
            NSString *file = [NSString stringWithFormat:@"%@/%@.jpg", fileDirectory, key];
            
            NSData *data = UIImageJPEGRepresentation(preview, 1);
            if (data)
            {
                NSLog(@"save to %@", file);
                [data writeToFile:file atomically:YES];
            }else
            {
                NSLog(@"data is nil!");
            }
            data = nil;

        }else
            self.mapPreview[key] = preview;
    }
}

@end
