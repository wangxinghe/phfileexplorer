//
//  OperationUpdatePreview.m
//  PHFileExplorer
//
//  Created by Ricol Wang on 6/07/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import "OperationUpdatePreview.h"
#import "ItemGMGridViewCell.h"
#import <PHCommon/utils.h>
#import "CacheCentre.h"
#import <UIImage+ImageResize.h>

@interface OperationUpdatePreview ()

{
    float timestamp;
    CGSize sizePreview;
}

@property (copy) NSString *filePath;
@property (copy) NSString *key;
@property (weak) id <OperationUpdatePreviewDelegate> delegate;
@property (weak) id <IUpdateGUIWithPreview> gui;

@end

@implementation OperationUpdatePreview

- (instancetype)initWithFilePath:(NSString *)filePath andTimestamp:(float)time andKey:(NSString *)key andSize:(CGSize)size andDelegate:(id <OperationUpdatePreviewDelegate>)delegate andInterface:(id<IUpdateGUIWithPreview>)theInterface
{
    self = [super init];
    if (self)
    {
        self.filePath = filePath;
        self.delegate = delegate;
        self.key = key;
        self.gui = theInterface;
        sizePreview = size;
        timestamp = time;
    }
    return self;
}

- (void)main
{
    UIImage *tmpImage = nil;
    
    NSString *strFileExtension = [self.filePath lastPathComponent];
    strFileExtension = [[utils getFileExtension:strFileExtension] uppercaseString];
    if ([strFileExtension isEqualToString:@"PDF"])
    {
        tmpImage = [utils getImageForPDF:self.filePath andPdfThumbSize:sizePreview];
    }else if ([strFileExtension isEqualToString:@"PNG"] || [strFileExtension isEqualToString:@"JPG"])
    {
        UIImage *image = [UIImage imageWithContentsOfFile:self.filePath];
        CGSize imageSize = image.size;
        if (imageSize.width > 0)
        {
            float ratio = imageSize.height / imageSize.width;
            CGSize preview = sizePreview;
            preview.height = ratio * preview.width;
            tmpImage = [image resizeImageWithnewSize:preview];
        }
    }
    
    if (tmpImage)
    {
        [utils runInMainThread:^(){
            [self.delegate operationUpdatePreviewComleteWith:tmpImage andKey:self.key];
            [self.gui updateGuiWithPreview:tmpImage withFilepath:self.filePath];
        }];
    }
}

@end
