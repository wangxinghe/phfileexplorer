//
//  OperationUpdatePreview.h
//  PHFileExplorer
//
//  Created by Ricol Wang on 6/07/2015.
//  Copyright (c) 2015 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OperationUpdatePreviewDelegate.h"
#import "IUpdateGUIWithPreview.h"

@interface OperationUpdatePreview : NSOperation

- (instancetype)initWithFilePath:(NSString *)filePath andTimestamp:(float)time andKey:(NSString *)key andSize:(CGSize)size andDelegate:(id <OperationUpdatePreviewDelegate>)delegate andInterface:(id <IUpdateGUIWithPreview>)theInterface;

@end
