//
//  FileExplorerViewController.m
//  PHFileExplorer
//
//  Created by Ricol Wang on 7/08/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import "FileExplorerViewController.h"
#import "WebContentViewController.h"
#import "PHFECommon.h"

@interface FileExplorerViewController ()

{
    int RowForIPadLandscape;
    int RowForIPadPortrait;
    int RowForIPhoneLandscape;
    int RowForIPhonePortrait;
}

@property (strong) NSString *root; //the title will be the current path without the root component.

@end

@implementation FileExplorerViewController

- (id)initWithPath:(NSString *)path andRoot:(NSString *)root andRowForIpadLandscape:(int)rowForIPadLandscape andRowForIPadPortrait:(int)rowForIPadPortrait andRowForIPhoneLandscape:(int)rowForIPhoneLandscape andRowForIPhonePortrait:(int)rowForIPhonePortrait;
{
    self = [super initWithNibName:@"FileExplorerViewController" bundle:nil];
    if (self)
    {
        // Custom initialization
        [self InitializeWithPath:path andRowForIpadLandscape:rowForIPadLandscape andRowForIPadPortrait:rowForIPadPortrait andRowForIPhoneLandscape:rowForIPhoneLandscape andRowForIPhonePortrait:rowForIPhonePortrait];
        self.root = root;
        RowForIPadLandscape = rowForIPadLandscape;
        RowForIPadPortrait = rowForIPadPortrait;
        RowForIPhoneLandscape = rowForIPhoneLandscape;
        RowForIPhonePortrait = rowForIPhonePortrait;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    //set the title
    NSArray *tmpArray = [self.currentPath componentsSeparatedByString:@"/"];
    NSMutableString *tmpMutableString = [[NSMutableString alloc] initWithString:@"LIBRARY:"];
    int tmpIndex = [tmpArray indexOfObject:@"Documents"];
    
    for (int i = tmpIndex; i < tmpArray.count; i++)
        [tmpMutableString appendFormat:@"/%@", tmpArray[i]];
  
    if (self.root)
    {
        NSRange range = [tmpMutableString rangeOfString:self.root];
        if (range.location != NSNotFound)
            [tmpMutableString deleteCharactersInRange:range];
    }
    
    NSString *strTitle = nil;
    
    if (self.datasoure)
        strTitle = [self.datasoure PHFileExplorerPathNameForPathName:tmpMutableString];
    
    if (!strTitle)
        strTitle = tmpMutableString;
        
    self.title = strTitle;
}

#pragma mark - Override Base Methods

- (void)ProcessDirectory:(NSURL *)url
{
    NSString *tmpFileName = [url lastPathComponent];
    NSString *tmpFileFullPath = [self.currentPath stringByAppendingFormat:@"/%@", tmpFileName];
    FileExplorerViewController *tmpVC  = [[FileExplorerViewController alloc] initWithPath:tmpFileFullPath andRoot:self.root andRowForIpadLandscape:RowForIPadLandscape andRowForIPadPortrait:RowForIPadPortrait andRowForIPhoneLandscape:RowForIPhoneLandscape andRowForIPhonePortrait:RowForIPhonePortrait];
    tmpVC.bAutoExtract = self.bAutoExtract;
    tmpVC.bDeleteOnceExtract = self.bDeleteOnceExtract;
    tmpVC.bShowLoadingBar = self.bShowLoadingBar;
    tmpVC.bHideCompressedFile = self.bHideCompressedFile;
    tmpVC.bPDFPreview = self.bPDFPreview;
    tmpVC.bImagePreview = self.bImagePreview;
    tmpVC.delegate = self.delegate;
    tmpVC.datasoure = self.datasoure;
    
    [self.navigationController pushViewController:tmpVC animated:YES];
}

@end
