//
//  PDFReader.h
//  JimsAntennas
//
//  Created by Leon on 12/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDFScrollViewSmall.h"
#import "PDFScroll.h"
#import <MessageUI/MessageUI.h>

@interface PDFReader : UIViewController <UIScrollViewDelegate, MFMailComposeViewControllerDelegate>{
	IBOutlet UILabel *titleLabel;
	IBOutlet UIView *topBar;
	IBOutlet UIScrollView *scrollView;
	NSString *bookTitle;
	NSString *currentPath;
	PDFScroll *svPage1;
	PDFScrollViewSmall *svPagePrev;
	PDFScrollViewSmall *svPageNext;
	NSInteger numOfPages;
	NSInteger currentPage;
	NSInteger prevPage;
	BOOL inLandscape;
	BOOL sliderDown;
	IBOutlet UIView *spinnerBack;
	IBOutlet UIActivityIndicatorView *spinner;
	
	CGPDFPageRef page;
	CGPDFDocumentRef pdf;
	CGPDFDocumentRef pdfBack;	
	CGPDFPageRef backPage;
	IBOutlet UIView *pageControllerBack;
	IBOutlet UILabel *totalPages;
	IBOutlet UILabel *currentPageLabel;
	IBOutlet UISlider *pageSlider;

	NSString *folderPath;
    IBOutlet UIImageView *headerImageView;
    IBOutlet UILabel *prepareLabel;
    
    BOOL shouldRotate;
    IBOutlet UIButton *lockButton;

}
@property(nonatomic)int scaleByValue;

@property(nonatomic, retain)IBOutlet UILabel *titleLabel;
@property(nonatomic, retain)IBOutlet UIView *topBar;
@property(nonatomic, retain)IBOutlet UIScrollView *scrollView;
@property(nonatomic, retain)NSString *bookTitle;
@property(nonatomic, retain)NSString *currentPath;
@property(nonatomic, retain)PDFScroll *svPage1;
@property(nonatomic, retain)PDFScrollViewSmall *svPagePrev;
@property(nonatomic, retain)PDFScrollViewSmall *svPageNext;

@property(nonatomic, retain)IBOutlet UIView *spinnerBack;
@property(nonatomic, retain)IBOutlet UIActivityIndicatorView *spinner;

@property(nonatomic, retain)IBOutlet UIView *pageControllerBack;
@property(nonatomic, retain)IBOutlet UILabel *totalPages;
@property(nonatomic, retain)IBOutlet UILabel *currentPageLabel;
@property(nonatomic, retain)IBOutlet UISlider *pageSlider;

@property(nonatomic, retain)NSString *folderPath;
@property (nonatomic, retain)IBOutlet UIImageView *headerImageView;
@property (nonatomic, retain)IBOutlet UILabel *prepareLabel;
@property (nonatomic)BOOL shouldRotate;
@property (nonatomic, retain)IBOutlet UIButton *lockButton;



-(IBAction)goBack;
-(void)sliderMoved;
-(IBAction)sliderMovedStart;
-(IBAction)sliderMovedFinished;
-(void)updateSlider;
-(void)cachePDFPages;
-(IBAction)emailPDF;
-(IBAction)lockRotation;
@end
