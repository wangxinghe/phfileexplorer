
#import "PDFScrollViewSmall.h"
#import "TiledPDFView.h"
#import <QuartzCore/QuartzCore.h>

@implementation PDFScrollViewSmall
@synthesize numPages;
- (void)dealloc
{
	
	// Clean up
	if(backgroundImageView){
		[backgroundImageView removeFromSuperview];

	}

}
- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) {
		self.backgroundColor = [UIColor blackColor];		
	}
    return self;
}

- (id)initWithFrame:(CGRect)frame withPage:(CGPDFPageRef)newPage
{
    if ((self = [super initWithFrame:frame])) {
		self.backgroundColor = [UIColor blackColor];		
    }
	/*	
	 // determine the size of the PDF page
	 CGRect pageRect = CGPDFPageGetBoxRect(newPage, kCGPDFMediaBox);
	 //pdfScale = 1.2;
	 pdfScale = (self.frame.size.width/pageRect.size.width);
	 
	 pageRect.size = CGSizeMake(pageRect.size.width*pdfScale, pageRect.size.height*pdfScale);
	 
	 // Create a low res image representation of the PDF page to display before the TiledPDFView
	 // renders its content.
	 UIGraphicsBeginImageContext(pageRect.size);
	 
	 // First fill the background with white.
	 CGContextSetRGBFillColor(UIGraphicsGetCurrentContext(), 1.0,1.0,1.0,1.0);
	 CGContextFillRect(UIGraphicsGetCurrentContext(),pageRect);
	 
	 CGContextSaveGState(UIGraphicsGetCurrentContext());
	 // Flip the context so that the PDF page is rendered
	 // right side up.
	 CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, pageRect.size.height);
	 CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
	 
	 // Scale the context so that the PDF page is rendered 
	 // at the correct size for the zoom level.
	 CGContextScaleCTM(UIGraphicsGetCurrentContext(), pdfScale,pdfScale);	
	 CGContextDrawPDFPage(UIGraphicsGetCurrentContext(), newPage);
	 CGContextRestoreGState(UIGraphicsGetCurrentContext());
	 
	 backgroundImageView = [[UIImageView alloc] initWithImage:UIGraphicsGetImageFromCurrentImageContext()];
	 backgroundImageView.frame = pageRect;
	 backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
	 [self addSubview:backgroundImageView];
	 [self sendSubviewToBack:backgroundImageView];
	 UIGraphicsEndImageContext();
	 
	 
	 */	
    return self;
}

- (id)initWithFrame:(CGRect)frame withPageNum:(NSInteger)newPageNum andPath:(NSString*)folderPath{
    if ((self = [super initWithFrame:frame])) {
		self.backgroundColor = [UIColor blackColor];		
    }
	NSString *imagePath = [NSString stringWithFormat:@"%@%d.jpg",folderPath,newPageNum];

	if([[NSFileManager defaultManager] fileExistsAtPath:imagePath]==YES){

		UIImage *imageThumb = [UIImage imageWithContentsOfFile:imagePath];
		CGImageRef imageCG = imageThumb.CGImage;
		NSUInteger width = CGImageGetWidth(imageCG);
		NSUInteger height = CGImageGetHeight(imageCG);
		
		if ([UIDevice currentDevice].orientation == UIInterfaceOrientationPortrait ||
			[UIDevice currentDevice].orientation == UIInterfaceOrientationPortraitUpsideDown)
		{
			if(height > width){
				pdfScale = (self.frame.size.height/height);
			}else{
				pdfScale = (self.frame.size.width/width);	
			}
			
			
		}else{
			pdfScale = (self.frame.size.width/width);
		}
		width = width * pdfScale;
		height = height * pdfScale;
		self.contentSize = CGSizeMake(width, height);	
		backgroundImageView = [[UIImageView alloc] initWithImage:imageThumb];
		backgroundImageView.frame = CGRectMake(0,0,width,height);
		backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
		[self addSubview:backgroundImageView];
		[self sendSubviewToBack:backgroundImageView];
		
		

	}
    return self;	
}

- (void)layoutSubviews 
{
    [super layoutSubviews];
    
    // center the image as it becomes smaller than the size of the screen
	
    CGSize boundsSize = self.bounds.size;
    CGRect frameToCenter = backgroundImageView.frame;
    
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width)
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    else
        frameToCenter.origin.x = 0;
    
    // center vertically
    if (frameToCenter.size.height < boundsSize.height)
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    else
        frameToCenter.origin.y = 0;

    backgroundImageView.frame=frameToCenter;

}


@end
