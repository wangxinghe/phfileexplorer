
#import <UIKit/UIKit.h>


@interface PDFScrollViewSmall : UIScrollView{

	UIImageView *backgroundImageView;
	
	// current pdf zoom scale
	CGFloat pdfScale;

	
}	
@property(nonatomic) NSInteger numPages;

- (id)initWithFrame:(CGRect)frame withPage:(CGPDFPageRef)newPage;
- (id)initWithFrame:(CGRect)frame withPageNum:(NSInteger)newPageNum andPath:(NSString*)folderPath;
@end
