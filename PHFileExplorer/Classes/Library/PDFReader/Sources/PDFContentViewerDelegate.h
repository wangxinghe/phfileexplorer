//
//  PDFContentViewerDelegate.h
//  Sidekick
//
//  Created by Ricol Wang on 17/09/13.
//
//

#import <Foundation/Foundation.h>

@class ReaderViewController;

@protocol PDFContentViewerDelegate <NSObject>

- (void)PDFContentViewerDoubleTapped:(ReaderViewController *)vc;

@end
