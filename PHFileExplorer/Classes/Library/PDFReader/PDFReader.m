//
//  PDFReader.m
//  JimsAntennas
//
//  Created by Leon on 12/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "PDFReader.h"
#import "PHFEHeader.h"

@interface PDFReader ()

@property int isRunningPDFCache;

@end

@implementation PDFReader
@synthesize titleLabel, topBar, scrollView, currentPath, bookTitle, svPage1, svPagePrev, svPageNext, spinnerBack, spinner,shouldRotate;
@synthesize pageControllerBack, totalPages, currentPageLabel, pageSlider, folderPath, headerImageView, prepareLabel,lockButton;

- (void)dealloc {
	CGPDFPageRelease(page);
	CGPDFDocumentRelease(pdf);
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	spinnerBack.hidden = NO;
    pageControllerBack.hidden = YES;
	[spinner startAnimating];
	self.titleLabel.text = bookTitle;
    shouldRotate = true;



//    if(appDelegate.hasHeader == 1){
//        headerImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@_theme/header_image.png",LIBRARY_DIRECTORY,appDelegate.divisionFolder]];
//    }else{
//        headerImageView.image = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:HEADERIMAGE ofType:@"png"]];
//    }
//    if(appDelegate.headerTextBlack == 1){
//        titleLabel.textColor = [UIColor blackColor];
//    }else{
//        titleLabel.textColor = [UIColor whiteColor];
//    }
    
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(preparePDF) userInfo:nil repeats:NO];  
}

-(void)preparePDF{
    prepareLabel.hidden = YES;
	spinnerBack.hidden = YES;
	[spinner stopAnimating];
    pageControllerBack.hidden = NO;
	self.isRunningPDFCache = 1;
	// Create our first PDFScrollView and add it to the view controller.
	currentPage = 1;
	inLandscape = YES;
	pageControllerBack.hidden = YES;
	CFURLRef pdfURL = CFURLCreateFromFileSystemRepresentation(NULL, (const UInt8 *)[currentPath UTF8String],[currentPath length], NO);
	pdf = CGPDFDocumentCreateWithURL(pdfURL);
	CFRelease(pdfURL);		
	// Get the PDF Page that we will be drawing
	CGPDFPageRelease(page);
	page = CGPDFDocumentGetPage(pdf, currentPage);
	CGPDFPageRetain(page);
	numOfPages = CGPDFDocumentGetNumberOfPages(pdf);	
	sliderDown = NO;
	
	NSArray *pathArray = [currentPath componentsSeparatedByString:@"/"];
	folderPath = [currentPath stringByReplacingOccurrencesOfString:[pathArray objectAtIndex:([pathArray count]-1)] withString:@""];
	//run in background, get image of each page
	//[self performSelectorInBackground:@selector(cachePDFPages) withObject:nil];	
	//
	
	svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,1024,704) withPage:page];
	
	
	[self.scrollView addSubview:svPage1];
    
	if(numOfPages > 1){
		CGPDFPageRelease(page);
		page = CGPDFDocumentGetPage(pdf, currentPage+1);
		CGPDFPageRetain(page);
		svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
		[self.scrollView addSubview:svPageNext];
	    self.scrollView.contentSize = CGSizeMake(1024 * 2, 704);
	}
    
	NSString *string = [[NSString alloc] initWithFormat:@"%d", numOfPages];
	totalPages.text = string;
	pageSlider.maximumValue = numOfPages;
	NSString *stringPage = [[NSString alloc] initWithFormat:@"%d", currentPage];
	currentPageLabel.text = stringPage;
	[pageSlider addTarget:self action:@selector(sliderMoved) forControlEvents:UIControlEventValueChanged];	
	
	UITapGestureRecognizer *tapGesture = nil;
	tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTouchesOne:)];
	tapGesture.cancelsTouchesInView = NO; tapGesture.delaysTouchesEnded = NO;
	tapGesture.numberOfTouchesRequired = 1; // One finger single tap
	tapGesture.numberOfTapsRequired = 1;
	//tapGesture.delegate = self;
	[self.view addGestureRecognizer:tapGesture];   
}

-(void)cachePDFPages{
	CGFloat pdfScale;
	CFURLRef pdfURL = CFURLCreateFromFileSystemRepresentation(NULL, (const UInt8 *)[currentPath UTF8String],[currentPath length], NO);
	
	for(int i=1;i<(numOfPages+1);i++){
		
		if(self.isRunningPDFCache == 0){
			break;
		}
		else{

			//first check if file exists at path
			NSString *imagePath = [[NSString alloc] initWithFormat:@"%@%d.jpg",folderPath,i];
			
			if([[NSFileManager defaultManager] fileExistsAtPath:imagePath]==YES){
				//do nothing
			}else{
				//create image
				pdfBack = CGPDFDocumentCreateWithURL(pdfURL);
				backPage = CGPDFDocumentGetPage(pdfBack, i);
				CGPDFPageRetain(backPage);
				CGRect pageRect = CGPDFPageGetBoxRect(backPage, kCGPDFMediaBox);
				
				if(pageRect.size.width > 768){
					pdfScale = (768/pageRect.size.width);
					pageRect.size = CGSizeMake(pageRect.size.width*pdfScale, pageRect.size.height*pdfScale);				
				}
				
				// Create a low res image representation of the PDF page to display before the TiledPDFView
				// renders its content.
				UIGraphicsBeginImageContext(pageRect.size);
				
				// First fill the background with white.
				//CGContextSetRGBFillColor(UIGraphicsGetCurrentContext(), 1.0,1.0,1.0,1.0);
				//CGContextFillRect(UIGraphicsGetCurrentContext(),pageRect);
				
				CGContextSaveGState(UIGraphicsGetCurrentContext());
				// Flip the context so that the PDF page is rendered
				// right side up.
				CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, pageRect.size.height);
				CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
				
				// Scale the context so that the PDF page is rendered 
				// at the correct size for the zoom level.
				//CGContextScaleCTM(UIGraphicsGetCurrentContext(), pdfScale,pdfScale);	
				CGContextDrawPDFPage(UIGraphicsGetCurrentContext(), backPage);
				
				CGContextRestoreGState(UIGraphicsGetCurrentContext());
				
				UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
				
				//save it to disk
				NSData *screenData = UIImageJPEGRepresentation(viewImage, 0.6);
				[screenData writeToFile:imagePath atomically:YES];
				UIGraphicsEndImageContext();
				CGPDFPageRelease(backPage);
				CGPDFDocumentRelease(pdfBack);
			}
			

			
		}
	}
	CFRelease(pdfURL);
	
}

- (void)handleTouchesOne:(UITapGestureRecognizer *)recognizer
{
	if(pageControllerBack.hidden == YES){
		pageControllerBack.hidden = NO;
	}else{
		pageControllerBack.hidden = YES;	
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return shouldRotate;
}

- (void)viewWillDisappear:(BOOL)animated {
	self.isRunningPDFCache = 0;
//	if([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationLandscapeRight){
//		[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationLandscapeRight];
//	}
//	else{
//		[[UIDevice currentDevice] setOrientation:UIInterfaceOrientationLandscapeLeft];		
//	}
	
}


- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	//	CGPDFDocumentRelease(pdf);
	//	CFURLRef pdfURL = CFURLCreateFromFileSystemRepresentation(NULL, [currentPath UTF8String],[currentPath length], NO);		
	//	pdf = CGPDFDocumentCreateWithURL(pdfURL);
	//	CFRelease(pdfURL);	
	if ([UIDevice currentDevice].orientation == UIInterfaceOrientationLandscapeLeft ||
		[UIDevice currentDevice].orientation == UIInterfaceOrientationLandscapeRight)
	{
		inLandscape = YES;
		spinnerBack.frame = CGRectMake(0,44,1024,704);
		topBar.frame = CGRectMake(0,0,1024,44);
		scrollView.frame = CGRectMake(0,44,1024,704);
		pageControllerBack.frame = CGRectMake(0,693,1024,55);	
        lockButton.frame = CGRectMake(864,6, 32, 32);

    
		if(currentPage == 1){
			if(numOfPages == 1){
				[svPage1 removeFromSuperview];
				
				CGPDFPageRelease(page);
				page = CGPDFDocumentGetPage(pdf, currentPage);
				CGPDFPageRetain(page);
				
				svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,1024,704) withPage:page];
				[self.scrollView addSubview:svPage1];				
			}else{
				
				[svPage1 removeFromSuperview];
				
				CGPDFPageRelease(page);
				page = CGPDFDocumentGetPage(pdf, currentPage);
				CGPDFPageRetain(page);
				
				svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,1024,704) withPage:page];
				[self.scrollView addSubview:svPage1];
				
				//CGPDFPageRelease(page);
				//page = CGPDFDocumentGetPage(pdf, currentPage+1);
				//CGPDFPageRetain(page);
				
				[svPageNext removeFromSuperview];
				svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
				
				[self.scrollView addSubview:svPageNext];
	
				
				self.scrollView.contentSize = CGSizeMake(1024 * 2, 704);
				[self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];	
			}
		}
		else if(currentPage == numOfPages){
			[svPage1 removeFromSuperview];
			
			CGPDFPageRelease(page);
			page = CGPDFDocumentGetPage(pdf, currentPage);
			CGPDFPageRetain(page);
			
			svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPage:page];
			[self.scrollView addSubview:svPage1];

			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage-1);
			//CGPDFPageRetain(page);
			
			[svPagePrev removeFromSuperview];
			svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,1024,704) withPageNum:(currentPage-1) andPath:folderPath];
			
			[self.scrollView addSubview:svPagePrev];
	
			
			self.scrollView.contentSize = CGSizeMake(1024 * 2, 704);
			[self.scrollView setContentOffset:CGPointMake(1024, 0) animated:NO];				
		}
		else{
			[svPage1 removeFromSuperview];
			
			CGPDFPageRelease(page);
			page = CGPDFDocumentGetPage(pdf, currentPage);
			CGPDFPageRetain(page);
			
			svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPage:page];
			[self.scrollView addSubview:svPage1];
	
			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage-1);
			//CGPDFPageRetain(page);
			
			[svPagePrev removeFromSuperview];
			svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,1024,704) withPageNum:(currentPage-1) andPath:folderPath];
			
			[self.scrollView addSubview:svPagePrev];
	
			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage+1);
			//CGPDFPageRetain(page);
			
			[svPageNext removeFromSuperview];
			svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(2048,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
			
			[self.scrollView addSubview:svPageNext];
		
			
			self.scrollView.contentSize = CGSizeMake(1024 * 3, 704);
			[self.scrollView setContentOffset:CGPointMake(1024, 0) animated:NO];			
		}
		
		
	}
	else
	{
		inLandscape = NO;
		spinnerBack.frame = CGRectMake(0,44,768,980);
		topBar.frame = CGRectMake(0,0,768,44);
		scrollView.frame = CGRectMake(0,44,768,980);
		pageControllerBack.frame = CGRectMake(0,949,768,55);	
        lockButton.frame = CGRectMake(602, 6, 32, 32);
		if(currentPage == 1){
			if(numOfPages == 1){
				[svPage1 removeFromSuperview];
				
				CGPDFPageRelease(page);
				page = CGPDFDocumentGetPage(pdf, currentPage);
				CGPDFPageRetain(page);
				
				svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,768,980) withPage:page];
				[self.scrollView addSubview:svPage1];
					
			}else{
				
				[svPage1 removeFromSuperview];
				
				CGPDFPageRelease(page);
				page = CGPDFDocumentGetPage(pdf, currentPage);
				CGPDFPageRetain(page);
				
				svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,768,980) withPage:page];
				[self.scrollView addSubview:svPage1];
	
				
				//CGPDFPageRelease(page);
				//page = CGPDFDocumentGetPage(pdf, currentPage+1);
				//CGPDFPageRetain(page);
				
				[svPageNext removeFromSuperview];
				svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(768,0,768,980) withPageNum:(currentPage+1) andPath:folderPath];
				
				[self.scrollView addSubview:svPageNext];
	
				
				self.scrollView.contentSize = CGSizeMake(768 * 2, 980);
				[self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];	
			}
		}
		else if(currentPage == numOfPages){
			[svPage1 removeFromSuperview];
			
			CGPDFPageRelease(page);
			page = CGPDFDocumentGetPage(pdf, currentPage);
			CGPDFPageRetain(page);
			
			svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(768,0,768,980) withPage:page];
			[self.scrollView addSubview:svPage1];

			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage-1);
			//CGPDFPageRetain(page);
			
			[svPagePrev removeFromSuperview];
			svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,768,980) withPageNum:(currentPage-1) andPath:folderPath];
			
			[self.scrollView addSubview:svPagePrev];

			
			self.scrollView.contentSize = CGSizeMake(768 * 2, 980);
			[self.scrollView setContentOffset:CGPointMake(768, 0) animated:NO];				
		}
		else{
			[svPage1 removeFromSuperview];
			
			CGPDFPageRelease(page);
			page = CGPDFDocumentGetPage(pdf, currentPage);
			CGPDFPageRetain(page);
			
			svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(768,0,768,980) withPage:page];
			[self.scrollView addSubview:svPage1];
	
			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage-1);
			//CGPDFPageRetain(page);
			
			[svPagePrev removeFromSuperview];
			svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,768,980) withPageNum:(currentPage-1) andPath:folderPath];
			
			[self.scrollView addSubview:svPagePrev];
	
			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage+1);
			//CGPDFPageRetain(page);
			
			[svPageNext removeFromSuperview];
			svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1536,0,768,980) withPageNum:(currentPage+1) andPath:folderPath];
			
			[self.scrollView addSubview:svPageNext];
		
			
			self.scrollView.contentSize = CGSizeMake(768 * 3, 980);
			[self.scrollView setContentOffset:CGPointMake(768, 0) animated:NO];			
		}
	}
}


- (void)didReceiveMemoryWarning {
	CGPDFDocumentRelease(pdf);
	CFURLRef pdfURL = CFURLCreateFromFileSystemRepresentation(NULL, (const UInt8 *)[currentPath UTF8String],[currentPath length], NO);
	pdf = CGPDFDocumentCreateWithURL(pdfURL);
	CFRelease(pdfURL);	
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
	
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
	
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	spinnerBack.hidden = NO;
	[spinner startAnimating];
	[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updatePages) userInfo:nil repeats:NO];
}

-(void)updatePages{
	CGPDFDocumentRelease(pdf);
	CFURLRef pdfURL = CFURLCreateFromFileSystemRepresentation(NULL, (const UInt8 *)[currentPath UTF8String],[currentPath length], NO);
	pdf = CGPDFDocumentCreateWithURL(pdfURL);
	CFRelease(pdfURL);	
	if(inLandscape == YES){
		//from first page
		if(currentPage == 1){
			//page went forward
			if(self.scrollView.contentOffset.x == 1024){
				currentPage = currentPage + 1;
				//if at final page
				if(currentPage == numOfPages){
					[svPage1 removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPage:page];
					[self.scrollView addSubview:svPage1];
		
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,1024,704) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
		
					
					self.scrollView.contentSize = CGSizeMake(1024 * 2, 704);
					[self.scrollView setContentOffset:CGPointMake(1024, 0) animated:NO];	
				}else{
					[svPage1 removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPage:page];
					[self.scrollView addSubview:svPage1];
	
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,1024,704) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
			
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(2048,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];

					
					self.scrollView.contentSize = CGSizeMake(1024 * 3, 704);
					[self.scrollView setContentOffset:CGPointMake(1024, 0) animated:NO];	
				}
				
			}			
		}
		//from last page
		else if(currentPage == numOfPages){
			//page went back
			if(self.scrollView.contentOffset.x == 0){
				currentPage = currentPage - 1;
				if(currentPage == 1){
					[svPage1 removeFromSuperview];
					[svPagePrev removeFromSuperview];
					
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,1024,704) withPage:page];
					[self.scrollView addSubview:svPage1];

					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];
	
					
					self.scrollView.contentSize = CGSizeMake(1024 * 2, 704);
					[self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];						
				}else{
					[svPage1 removeFromSuperview];
					
					[svPagePrev removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPage:page];
					[self.scrollView addSubview:svPage1];
		
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,1024,704) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
	
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(2048,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];
	
					
					self.scrollView.contentSize = CGSizeMake(1024 * 3, 704);
					[self.scrollView setContentOffset:CGPointMake(1024, 0) animated:NO];	
				}
			}		
		}
		else{
			//page went back
			if(self.scrollView.contentOffset.x == 0){
				currentPage = currentPage - 1;
				if(currentPage == 1){
					[svPage1 removeFromSuperview];
					[svPagePrev removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,1024,704) withPage:page];
					[self.scrollView addSubview:svPage1];
		
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];

					
					self.scrollView.contentSize = CGSizeMake(1024 * 2, 704);
					[self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];						
				}else{
					[svPage1 removeFromSuperview];
					[svPagePrev removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPage:page];
					[self.scrollView addSubview:svPage1];

					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,1024,704) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
		
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(2048,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];

					
					self.scrollView.contentSize = CGSizeMake(1024 * 3, 704);
					[self.scrollView setContentOffset:CGPointMake(1024, 0) animated:NO];					
				}
			}
			//page went forward
			else if(self.scrollView.contentOffset.x == 2048){
				currentPage = currentPage + 1;
				if(currentPage == numOfPages){
					[svPage1 removeFromSuperview];
					[svPagePrev removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPage:page];
					[self.scrollView addSubview:svPage1];

					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,1024,704) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
		
					
					self.scrollView.contentSize = CGSizeMake(1024 * 2, 704);
					[self.scrollView setContentOffset:CGPointMake(1024, 0) animated:NO];	
				}else{
					[svPage1 removeFromSuperview];
					[svPagePrev removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPage:page];
					[self.scrollView addSubview:svPage1];
		
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,1024,704) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
	
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(2048,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];

					
					self.scrollView.contentSize = CGSizeMake(1024 * 3, 704);
					[self.scrollView setContentOffset:CGPointMake(1024, 0) animated:NO];					
				}
			}
		}
	}else if(inLandscape == NO){
		//from first page
		if(currentPage == 1){
			//page went forward
			if(self.scrollView.contentOffset.x == 768){
				currentPage = currentPage + 1;
				//if at final page
				if(currentPage == numOfPages){
					[svPage1 removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(768,0,768,980) withPage:page];
					[self.scrollView addSubview:svPage1];

					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,768,980) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
			
					
					self.scrollView.contentSize = CGSizeMake(768 * 2, 980);
					[self.scrollView setContentOffset:CGPointMake(768, 0) animated:NO];	
				}else{
					[svPage1 removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(768,0,768,980) withPage:page];
					[self.scrollView addSubview:svPage1];

					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,768,980) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
				
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1536,0,768,980) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];
	
					
					self.scrollView.contentSize = CGSizeMake(768 * 3, 980);
					[self.scrollView setContentOffset:CGPointMake(768, 0) animated:NO];	
				}
				
			}			
		}
		//from last page
		else if(currentPage == numOfPages){
			//page went back
			if(self.scrollView.contentOffset.x == 0){
				currentPage = currentPage - 1;
				if(currentPage == 1){
					[svPage1 removeFromSuperview];
					[svPagePrev removeFromSuperview];
					
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,768,980) withPage:page];
					[self.scrollView addSubview:svPage1];

					
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(768,0,768,980) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];
		
					
					self.scrollView.contentSize = CGSizeMake(768 * 2, 980);
					[self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];						
				}else{
					[svPage1 removeFromSuperview];
					
					[svPagePrev removeFromSuperview];
					
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(768,0,768,980) withPage:page];
					[self.scrollView addSubview:svPage1];
		
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,768,980) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
	
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1536,0,768,980) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];
		
					
					self.scrollView.contentSize = CGSizeMake(768 * 3, 980);
					[self.scrollView setContentOffset:CGPointMake(768, 0) animated:NO];	
				}
			}		
		}
		else{
			//page went back
			if(self.scrollView.contentOffset.x == 0){
				currentPage = currentPage - 1;
				if(currentPage == 1){
					[svPage1 removeFromSuperview];
					[svPagePrev removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,768,980) withPage:page];
					[self.scrollView addSubview:svPage1];

					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(768,0,768,980) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];
		
					
					self.scrollView.contentSize = CGSizeMake(768 * 2, 980);
					[self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];					
				}else{
					[svPage1 removeFromSuperview];
					[svPagePrev removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(768,0,768,980) withPage:page];
					[self.scrollView addSubview:svPage1];
		
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,768,980) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
		
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1536,0,768,980) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];
	
					
					self.scrollView.contentSize = CGSizeMake(768 * 3, 980);
					[self.scrollView setContentOffset:CGPointMake(768, 0) animated:NO];					
				}
			}
			//page went forward
			else if(self.scrollView.contentOffset.x == 1536){
				currentPage = currentPage + 1;
				if(currentPage == numOfPages){
					[svPage1 removeFromSuperview];
					[svPagePrev removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(768,0,768,980) withPage:page];
					[self.scrollView addSubview:svPage1];
		
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,768,980) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
	
					
					
					self.scrollView.contentSize = CGSizeMake(768 * 2, 980);
					[self.scrollView setContentOffset:CGPointMake(768, 0) animated:NO];	
				}else{
					[svPage1 removeFromSuperview];
					[svPagePrev removeFromSuperview];
					[svPageNext removeFromSuperview];
					
					CGPDFPageRelease(page);
					page = CGPDFDocumentGetPage(pdf, currentPage);
					CGPDFPageRetain(page);
					
					svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(768,0,768,980) withPage:page];
					[self.scrollView addSubview:svPage1];

					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage-1);
					//CGPDFPageRetain(page);
					
					svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,768,980) withPageNum:(currentPage-1) andPath:folderPath];
					
					[self.scrollView addSubview:svPagePrev];
				
					
					//CGPDFPageRelease(page);
					//page = CGPDFDocumentGetPage(pdf, currentPage+1);
					//CGPDFPageRetain(page);
					
					svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1536,0,768,980) withPageNum:(currentPage+1) andPath:folderPath];
					
					[self.scrollView addSubview:svPageNext];
		
					
					self.scrollView.contentSize = CGSizeMake(768 * 3, 980);
					[self.scrollView setContentOffset:CGPointMake(768, 0) animated:NO];						
				}
			}
		}
	}
	[self updateSlider];
	[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(stopSpinner) userInfo:nil repeats:NO];	
	
}

-(void)updateSlider{
	NSString *newCurrent = [[NSString alloc]initWithFormat:@"%d", currentPage];
	currentPageLabel.text = newCurrent;

	pageSlider.value = currentPage;
}

-(void)stopSpinner{
	spinnerBack.hidden = YES;
	[spinner stopAnimating];	
}


-(void)sliderMoved{
	NSString *stringPage = [[NSString alloc] initWithFormat:@"%i", (int)pageSlider.value];
	currentPageLabel.text = stringPage;
	
}
-(IBAction)sliderMovedStart{
	sliderDown = YES;
}
-(IBAction)sliderMovedFinished{
	if(sliderDown == NO){
		return;
	}
	
	sliderDown = NO;
	if(currentPage != (int)pageSlider.value){
		spinnerBack.hidden = NO;
		[spinner startAnimating];
		prevPage = currentPage;
		currentPage = (int)pageSlider.value;	
		
		[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(changePage) userInfo:nil repeats:NO];	
	}
	
}
-(void)changePage{
	CGPDFDocumentRelease(pdf);
	CFURLRef pdfURL = CFURLCreateFromFileSystemRepresentation(NULL, (const UInt8 *)[currentPath UTF8String],[currentPath length], NO);
	pdf = CGPDFDocumentCreateWithURL(pdfURL);
	CFRelease(pdfURL);	
	if(inLandscape == YES){
		
		if(prevPage == 1){
			[svPage1 removeFromSuperview];
			[svPageNext removeFromSuperview];				
		}
		else if(prevPage == numOfPages){
			[svPage1 removeFromSuperview];
			[svPagePrev removeFromSuperview];			
		}
		else {
			[svPage1 removeFromSuperview];
			[svPageNext removeFromSuperview];
			[svPagePrev removeFromSuperview];	
		}
		
		if(currentPage == 1){
			CGPDFPageRelease(page);
			page = CGPDFDocumentGetPage(pdf, currentPage);
			CGPDFPageRetain(page);
			
			svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,1024,704) withPage:page];
			[self.scrollView addSubview:svPage1];
	
			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage+1);
			//CGPDFPageRetain(page);
			
			svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
			
			[self.scrollView addSubview:svPageNext];

			
			self.scrollView.contentSize = CGSizeMake(1024 * 2, 704);
			[self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];									
		}else if(currentPage == numOfPages){
			CGPDFPageRelease(page);
			page = CGPDFDocumentGetPage(pdf, currentPage);
			CGPDFPageRetain(page);
			
			svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPage:page];
			[self.scrollView addSubview:svPage1];

			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage-1);
			//CGPDFPageRetain(page);
			
			svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,1024,704) withPageNum:(currentPage-1) andPath:folderPath];
			
			[self.scrollView addSubview:svPagePrev];
			
			
			self.scrollView.contentSize = CGSizeMake(1024 * 2, 704);
			[self.scrollView setContentOffset:CGPointMake(1024, 0) animated:NO];				
		}else{
			CGPDFPageRelease(page);
			page = CGPDFDocumentGetPage(pdf, currentPage);
			CGPDFPageRetain(page);
			
			svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(1024,0,1024,704) withPage:page];
			[self.scrollView addSubview:svPage1];

			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage-1);
			//CGPDFPageRetain(page);
			
			svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,1024,704) withPageNum:(currentPage-1) andPath:folderPath];
			
			[self.scrollView addSubview:svPagePrev];
	
			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage+1);
			//CGPDFPageRetain(page);
			
			svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(2048,0,1024,704) withPageNum:(currentPage+1) andPath:folderPath];
			
			[self.scrollView addSubview:svPageNext];
	
			
			self.scrollView.contentSize = CGSizeMake(1024 * 3, 704);
			[self.scrollView setContentOffset:CGPointMake(1024, 0) animated:NO];			
		}
	}else{
		
		if(prevPage == 1){
			[svPage1 removeFromSuperview];
			[svPageNext removeFromSuperview];				
		}
		else if(prevPage == numOfPages){
			[svPage1 removeFromSuperview];
			[svPagePrev removeFromSuperview];			
		}
		else {
			[svPage1 removeFromSuperview];
			[svPageNext removeFromSuperview];
			[svPagePrev removeFromSuperview];	
		}	
		if(currentPage == 1){
			CGPDFPageRelease(page);
			page = CGPDFDocumentGetPage(pdf, currentPage);
			CGPDFPageRetain(page);
			
			svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(0,0,768,980) withPage:page];
			[self.scrollView addSubview:svPage1];

			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage+1);
			//CGPDFPageRetain(page);
			
			svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(768,0,768,980) withPageNum:(currentPage+1) andPath:folderPath];
			
			[self.scrollView addSubview:svPageNext];

			
			self.scrollView.contentSize = CGSizeMake(768 * 2, 980);
			[self.scrollView setContentOffset:CGPointMake(0, 0) animated:NO];								
		}else if(currentPage == numOfPages){
			CGPDFPageRelease(page);
			page = CGPDFDocumentGetPage(pdf, currentPage);
			CGPDFPageRetain(page);
			
			svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(768,0,768,980) withPage:page];
			[self.scrollView addSubview:svPage1];
	
			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage-1);
			//CGPDFPageRetain(page);
			
			svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,768,980) withPageNum:(currentPage-1) andPath:folderPath];
			
			[self.scrollView addSubview:svPagePrev];
		
			
			self.scrollView.contentSize = CGSizeMake(768 * 2, 980);
			[self.scrollView setContentOffset:CGPointMake(768, 0) animated:NO];				
		}else{
			CGPDFPageRelease(page);
			page = CGPDFDocumentGetPage(pdf, currentPage);
			CGPDFPageRetain(page);
			
			svPage1 = [[PDFScroll alloc] initWithFrame:CGRectMake(768,0,768,980) withPage:page];
			[self.scrollView addSubview:svPage1];
	
			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage-1);
			//CGPDFPageRetain(page);
			
			svPagePrev = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(0,0,768,980) withPageNum:(currentPage-1) andPath:folderPath];
			
			[self.scrollView addSubview:svPagePrev];
				
			
			//CGPDFPageRelease(page);
			//page = CGPDFDocumentGetPage(pdf, currentPage+1);
			//CGPDFPageRetain(page);
			
			svPageNext = [[PDFScrollViewSmall alloc] initWithFrame:CGRectMake(1536,0,768,980) withPageNum:(currentPage+1) andPath:folderPath];
			
			[self.scrollView addSubview:svPageNext];
	
			
			self.scrollView.contentSize = CGSizeMake(768 * 3, 980);
			[self.scrollView setContentOffset:CGPointMake(768, 0) animated:NO];					
		}
	}
	[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(stopSpinner) userInfo:nil repeats:NO];	
}

-(IBAction)goBack{
	spinnerBack.hidden = NO;
	spinnerBack.backgroundColor = [UIColor blackColor];
	[spinner startAnimating];
	[svPage1 removeFromSuperview];
	
	if(currentPage == 1){
		
		[svPageNext removeFromSuperview];		
	}else if(currentPage == numOfPages){
		[svPagePrev removeFromSuperview];
		
	}else{
		[svPagePrev removeFromSuperview];
		[svPageNext removeFromSuperview];	
	}
	
	
	
	[NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(actuallyGoBack) userInfo:nil repeats:NO];
}
-(void)actuallyGoBack{
	[self.navigationController popViewControllerAnimated:YES];	
}

-(IBAction)emailPDF{
    if([MFMailComposeViewController canSendMail]==TRUE){
        

        
        NSArray *pathArray = [currentPath componentsSeparatedByString:@"/"];
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
        
        [picker setSubject:[pathArray objectAtIndex:([pathArray count]-1)]];
        
        [picker setMessageBody:@"" isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
        
        picker.navigationBar.barStyle = UIBarStyleDefault; // choose your style, unfortunately, Translucent colors behave quirky.
        
        
        NSData *csvData = [NSData dataWithContentsOfFile:currentPath]; 
        [picker addAttachmentData:csvData mimeType:@"application/pdf" fileName:[pathArray objectAtIndex:([pathArray count]-1)]];
        
        [self presentModalViewController:picker animated:YES];

    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MESSAGE" message:@"No Email Account Setup!\nYou must setup an email account in order to use this feature."
                                                       delegate:self 
                                              cancelButtonTitle:nil 
                                              otherButtonTitles:@"OK",nil];
        [alert show];
   
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{ 
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
			break;
			
		default:
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Sending Failed - Unknown Error - Please check your mailbox setup"
														   delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[alert show];

		}
			
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

-(IBAction)lockRotation{
    shouldRotate = !shouldRotate;
    if(shouldRotate){
        [lockButton setImage:[UIImage imageNamed:@"unlock.png"] forState:UIControlStateNormal];
    }
    
    else{
        [lockButton setImage:[UIImage imageNamed:@"lock.png"] forState:UIControlStateNormal];


    }
}

@end
