//
//  PHFileExplorerDataSource.h
//  PHFileExplorer
//
//  Created by Ricol Wang on 3/09/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PHFileExplorerDataSource <NSObject>

- (UIImage *)PHFileExplorerImageForFileName:(NSString *)filename;
- (UIImage *)PHFileExplorerImageForDirectory:(NSString *)directory;
- (NSString *)PHFileExplorerFileNameForFileName:(NSString *)filename;
- (NSString *)PHFileExplorerPathNameForPathName:(NSString *)pathName;
- (UIImage *)PHFileExplorerImageBackgroundForDirectory:(NSString *)directory;

@end
