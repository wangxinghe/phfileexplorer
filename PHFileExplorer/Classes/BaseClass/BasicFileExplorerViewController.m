//
//  BasicFileExplorerViewController.m
//  Sidekick
//
//  Created by Ricol Wang on 28/06/13.
//
//

#import "BasicFileExplorerViewController.h"
#import <SGDirWatchdog.h>
#import <PHCommon/utils.h>
#import <InforDialog.h>
#import "PHFECommon.h"
#import "PHFEHeader.h"
#import "ZipArchive.h"
#import "PDFReader.h"
#import "MediaPlayerViewController.h"
#import "ImageViewerViewController.h"
#import "DefaultViewerController.h"
#import "ExcelViewerViewController.h"
#import "WebContentViewController.h"
#import "ReaderDocument.h"
#import "ReaderViewController.h"
#import <PHAlertViewWithObject.h>
#import "PHLoadingViewController.h"
#import "PhotoBrowserViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CacheCentre.h"

#define X_GRIDVIEW 5
#define Y_GRIDVIEW 5

#define DELTA 5
#define COUNT_ROW_IPAD_LANDSCAPE 6
#define COUNT_ROW_IPAD_PORTRAIT 3
#define COUNT_ROW_IPHONE_LANDSCAPE 3
#define COUNT_ROW_IPHONE_PORTRAIT 2
#define ITEM_SPACE_IPAD 30
#define ITEM_SPACE_IPHONE 10

#define GAP_BOTTOM_LOADINGBAR 20

@interface BasicFileExplorerViewController () <GMGridViewDataSource, GMGridViewActionDelegate, ReaderViewControllerDelegate, PDFContentViewerDelegate, ItemGMGridViewCellDelegate, MBProgressHUDDelegate>

@property (strong) SGDirWatchdog *directoryWatchdog;
@property (strong) NSArray *arrayAllItems;
@property (strong) PHLoadingViewController *loadingVC;
@property BOOL bIsProcessingCompressedFiles;
@property BOOL bDirectoryExists;
@property (strong) UIBarButtonItem *barBtnCancel;
@property (strong) NSFileManager *fm;

- (void)DirectoryContentChanged:(BOOL)bAutoExtract;
- (void)ShowLoadingBar;
- (void)HideLoadingBar;
- (void)UpdateTheGridViewSize;

@end

@implementation BasicFileExplorerViewController

#pragma mark - View Life Cycle

- (void)InitializeWithPath:(NSString *)path andRowForIpadLandscape:(int)rowForIPadLandscape andRowForIPadPortrait:(int)rowForIPadPortrait andRowForIPhoneLandscape:(int)rowForIPhoneLandscape andRowForIPhonePortrait:(int)rowForIPhonePortrait
{
    self.currentPath = path;
    
    self.fm = [NSFileManager defaultManager];
    BOOL bDirectory;
    self.bDirectoryExists = [self.fm fileExistsAtPath:self.currentPath isDirectory:&bDirectory];
    self.bIsProcessingCompressedFiles = NO;
    self.itemsIPadLandscape = rowForIPadLandscape > 0 ? rowForIPadLandscape : COUNT_ROW_IPAD_LANDSCAPE;
    self.itemsIPadPortrait = rowForIPadPortrait > 0 ? rowForIPadPortrait : COUNT_ROW_IPAD_PORTRAIT;
    self.itemsIPhoneLandscape = rowForIPhoneLandscape > 0 ? rowForIPhoneLandscape : COUNT_ROW_IPHONE_LANDSCAPE;
    self.itemsIPhonePortrait = rowForIPhonePortrait > 0 ? rowForIPhonePortrait : COUNT_ROW_IPHONE_PORTRAIT;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //add loading bar on the bottom
    self.loadingVC = [[PHLoadingViewController alloc] init];
    self.loadingVC.view.frame = CGRectMake((self.view.bounds.size.width - self.loadingVC.view.bounds.size.width) / 2, self.view.bounds.size.height - self.loadingVC.view.bounds.size.height - GAP_BOTTOM_LOADINGBAR, self.loadingVC.view.bounds.size.width, self.loadingVC.view.bounds.size.height);
    self.loadingVC.view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    self.loadingVC.view.hidden = YES;
    [self.view addSubview:self.loadingVC.view];
    
    //check whether the directory exists. if not create the directory.
    if (!self.bDirectoryExists)
    {
        NSFileManager *fm = [NSFileManager defaultManager];
        NSError *error = nil;
        if (![fm createDirectoryAtPath:self.currentPath withIntermediateDirectories:YES attributes:nil error:&error])
        {
            [InforDialog showInforWithStatus:error.localizedDescription DisplayTime:PHFE_TIME_INFORDIALOG];
        }
    }
    
    if (!self.bDirectoryExists) return;
    
    //set directory watch dog and start to monitor the current path
    self.directoryWatchdog = [[SGDirWatchdog alloc] initWithPath:self.currentPath update:^(){
        [self DirectoryContentChanged:self.bAutoExtract];
    }];
    
    //set gridview
    self.gridViewContent.dataSource = self;
    self.gridViewContent.actionDelegate = self;
    self.gridViewContent.centerGrid = NO;
    
    if ([utils isIPhone])
        self.gridViewContent.itemSpacing = ITEM_SPACE_IPHONE;
    else
        self.gridViewContent.itemSpacing = ITEM_SPACE_IPAD;
    
    [self.gridViewContent reloadData];
    
    if ([utils isIOS7])
    {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
    }
    
#ifdef _DEBUG
    self.gridViewContent.backgroundColor = [utils randomColor];
#endif
    
    self.barBtnCancel = [[UIBarButtonItem alloc] initWithTitle:String(@"Cancel") style:UIBarButtonItemStyleDone target:self action:@selector(barBtnCancelOnTapped:)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self UpdateTheGridViewSize];
    
    UIImage *tmpImage = [self.datasoure PHFileExplorerImageBackgroundForDirectory:self.currentPath];
    if (tmpImage)
        self.imageViewBackground.image = tmpImage;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.bDirectoryExists)
    {
        [self.directoryWatchdog start];
        [self DirectoryContentChanged:self.bAutoExtract];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (self.bDirectoryExists)
        [self.directoryWatchdog stop];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self.directoryWatchdog stop];
    [self setImageViewBackground:nil];
    [self setGridViewContent:nil];
    [super viewDidUnload];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [UIView animateWithDuration:0.1 animations:^(){
        self.gridViewContent.alpha = 0;
    }];
    
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    [self UpdateTheGridViewSize];
    
    [UIView animateWithDuration:0.1 animations:^(){
        self.gridViewContent.alpha = 1;
    }];
}

#pragma mark - GMGridViewDataSource

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    ItemGMGridViewCell *cell = (ItemGMGridViewCell *)[gridView dequeueReusableCell];
    
    if (!cell)
    {
        cell = [[ItemGMGridViewCell alloc] initWithImage:[UIImage imageNamed:@""] andTitle:@"title"];
        cell.reuseIdentifier = @"ItemGMGridViewCell_Identifier";
        cell.delegate = self;
    }
    
    NSString *tmpStrTitle;
    UIImage *tmpImage;
    
    NSURL *tmpFileNameFullURL = self.arrayAllAvaliableItems[index];
    NSString *tmpFileNameFullPath = [self.currentPath stringByAppendingFormat:@"/%@", [tmpFileNameFullURL lastPathComponent]];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    BOOL dir;
    
    
    if ([fm fileExistsAtPath:tmpFileNameFullPath isDirectory:&dir])
    {
        if (dir)
        {
            if (self.datasoure)
                tmpImage = [self.datasoure PHFileExplorerImageForDirectory:tmpFileNameFullPath];
            
            if (!tmpImage)
                tmpImage = [UIImage imageNamed:@"Folder.png"];
        }else
        {
            if (self.datasoure)
                tmpImage = [self.datasoure PHFileExplorerImageForFileName:tmpFileNameFullPath];
            
            if (!tmpImage)
            {
                NSArray *tmpArray = [tmpFileNameFullPath componentsSeparatedByString:@"."];
                NSString *tmpFileExtension = [tmpArray lastObject];
                NSString *tmpStr_FileType = [PHFECommon getTypeFromExtension:tmpFileExtension];
                tmpStr_FileType = [tmpStr_FileType uppercaseString];
                
                if ([tmpStr_FileType isEqualToString:@"PDF"])
                {
                    tmpImage = [UIImage imageNamed:@"PDF"];
                }else if ([tmpStr_FileType isEqualToString:@"COMPRESSED"])
                {
                    tmpImage = [UIImage imageNamed:@"Zip"];
                }else if ([tmpStr_FileType isEqualToString:@"VIDEO"])
                {
                    tmpImage = [UIImage imageNamed:@"Video"];
                }else if ([tmpStr_FileType isEqualToString:@"IMAGE"])
                {
                    tmpImage = [UIImage imageNamed:@"Image"];
                }else if ([tmpStr_FileType isEqualToString:@"WEB"])
                {
                    tmpImage = [UIImage imageNamed:@"Web"];
                }else if ([tmpStr_FileType isEqualToString:@"SPREADSHEET"])
                {
                    tmpImage = [UIImage imageNamed:@"Excel"];
                }else if ([tmpStr_FileType isEqualToString:@"WORD"])
                {
                    tmpImage = [UIImage imageNamed:@"Word"];
                }else if ([tmpStr_FileType isEqualToString:@"POWERPOINT"])
                {
                    tmpImage = [UIImage imageNamed:@"Powerpoint"];
                }else if ([tmpStr_FileType isEqualToString:@"RSS"])
                {
                    tmpImage = [UIImage imageNamed:@"RSS"];
                }else if ([tmpStr_FileType isEqualToString:@"XML"])
                {
                    tmpImage = [UIImage imageNamed:@"XML"];
                }else
                {
                    tmpImage = [UIImage imageNamed:@"Unknown"];
                }
            }
        }
        
        tmpStrTitle = [PHFECommon getFileNameFromFullPath:tmpFileNameFullPath];
        tmpStrTitle = [PHFECommon getFileNameWithoutExtension:tmpStrTitle];
        
        if (self.datasoure)
            tmpStrTitle = [self.datasoure PHFileExplorerFileNameForFileName:tmpStrTitle];
    }else
        NSLog(@"File not exists.");
    
    [self ConfigureCell:cell withImage:tmpImage withTitle:tmpStrTitle andFullPath:tmpFileNameFullPath];
    
    return cell;
}

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return self.arrayAllAvaliableItems.count;
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    float size = 0;
    
    if ([utils isIPhone])
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
            size = (self.gridViewContent.bounds.size.width - (self.itemsIPhoneLandscape - 1) * ITEM_SPACE_IPHONE - 2 * DELTA) / self.itemsIPhoneLandscape;
        else
            size = (self.gridViewContent.bounds.size.width - (self.itemsIPhonePortrait - 1) * ITEM_SPACE_IPHONE - 2 * DELTA) / self.itemsIPhonePortrait;
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
            size = (self.gridViewContent.bounds.size.width - (self.itemsIPadLandscape - 1) * ITEM_SPACE_IPAD - 2 * DELTA) / self.itemsIPadLandscape;
        else
            size = (self.gridViewContent.bounds.size.width - (self.itemsIPadPortrait - 1) * ITEM_SPACE_IPAD - 2 * DELTA) / self.itemsIPadPortrait;
    }
    
    return CGSizeMake(size, size);
}

- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return YES;
}

#pragma mark - GMGridViewActionDelegate

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    BOOL isDir = YES;
    
    NSURL *tmpFileFullPathURL = self.arrayAllAvaliableItems[position];
    NSString *tmpFileName = [tmpFileFullPathURL lastPathComponent];
    NSString *tmpFileFullPath = [self.currentPath stringByAppendingFormat:@"/%@", tmpFileName];
    
    [[NSFileManager defaultManager] fileExistsAtPath:tmpFileFullPath isDirectory:&isDir];
    
    if(isDir)
        [self ProcessDirectory:tmpFileFullPathURL];
    else
        [self ProcessFile:tmpFileFullPathURL];
}

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSURL *tmpFileURL = self.arrayAllAvaliableItems[index];
    NSString *tmpFileNameFullPath =[self.currentPath stringByAppendingFormat:@"/%@", tmpFileURL.lastPathComponent];
    
    BOOL isDir = NO;
    
    if ([fm fileExistsAtPath:tmpFileNameFullPath isDirectory:&isDir])
    {
        PHAlertViewWithObject *tmpAlertView;
        
        if (isDir)
        {
            //try to delete the whole directory
            tmpAlertView = [[PHAlertViewWithObject alloc] initWithTitle:String(@"Warning") message:String(@"ConfirmDeleteDirectory") delegate:self cancelButtonTitle:String(@"Yes") otherButtonTitles:String(@"No"), nil];
            
        }else
        {
            //try to delete a single file
            tmpAlertView = [[PHAlertViewWithObject alloc] initWithTitle:String(@"Warning") message:String(@"CofirmDeleteFile") delegate:self cancelButtonTitle:String(@"Yes") otherButtonTitles:String(@"No"), nil];
        }
        
        tmpAlertView.tag = PHFE_TAG_ALERTVIEW_DELETE;
        tmpAlertView.object = tmpFileNameFullPath;
        [tmpAlertView show];
        
    }else
    {
        NSString *tmpStrMessage = [NSString stringWithFormat:@"%@: \n%@", String(@"UnexpectedError"), String(@"FileNotExist")];
        [InforDialog showErrorWithStatus:tmpStrMessage DisplayTime:PHFE_TIME_INFORDIALOG];
    }
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    self.gridViewContent.editing = NO;
}

#pragma mark - Override Methods

- (void)setItemsIPadLandscape:(int)itemsIPadLandscape
{
    if (_itemsIPadLandscape != itemsIPadLandscape)
    {
        _itemsIPadLandscape = itemsIPadLandscape;
        [self.gridViewContent reloadData];
    }
}

- (void)setItemsIPadPortrait:(int)itemsIPadPortrait
{
    if (_itemsIPadPortrait != itemsIPadPortrait)
    {
        _itemsIPadPortrait = itemsIPadPortrait;
        [self.gridViewContent reloadData];
    }
}

- (void)setItemsIPhonePortrait:(int)itemsIPhonePortrait
{
    if (_itemsIPhonePortrait != itemsIPhonePortrait)
    {
        _itemsIPhonePortrait = itemsIPhonePortrait;
        [self.gridViewContent reloadData];
    }
}

- (void)setItemsIPhoneLandscape:(int)itemsIPhoneLandscape
{
    if (_itemsIPhoneLandscape != itemsIPhoneLandscape)
    {
        _itemsIPhoneLandscape = itemsIPhoneLandscape;
        [self.gridViewContent reloadData];
    }
}

#pragma mark - Private Methods

- (void)barBtnCancelOnTapped:(id)sender
{
    self.gridViewContent.editing = NO;
    
    NSArray *tmpArray = self.navigationItem.rightBarButtonItems;
    NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] initWithArray:tmpArray];
    [tmpMutableArray removeObject:self.barBtnCancel];
    self.navigationItem.rightBarButtonItems = tmpMutableArray;
}

#pragma mark - Public Methods

- (void)ConfigureCell:(ItemGMGridViewCell *)cell withImage:(UIImage *)image withTitle:(NSString *)title andFullPath:(NSString *)fullpath
{
    [cell NewTitle:title];
    
    cell.filepath = fullpath;
    
    NSString *strFileExtension = [fullpath lastPathComponent];
    strFileExtension = [[utils getFileExtension:strFileExtension] uppercaseString];
    if ([strFileExtension isEqualToString:@"PDF"])
    {
        if (self.bPDFPreview)
        {
            [[CacheCentre sharedInstance] requestPreviewFilePath:fullpath andInterface:cell withDefaultImage:image];
        }else
            [cell NewImage:image];
    }else if ([strFileExtension isEqualToString:@"JPG"] || [strFileExtension isEqualToString:@"PNG"])
    {
        if (self.bImagePreview)
        {
            [[CacheCentre sharedInstance] requestPreviewFilePath:fullpath andInterface:cell withDefaultImage:image];
        }else
            [cell NewImage:image];
    }else
        [cell NewImage:image];
}

- (void)DirectoryContentChanged:(BOOL)bAutoExtract
{
    if (self.bIsProcessingCompressedFiles) return;
    
    [utils runAfter:0.1 andBlock:^(){
        [self ShowLoadingBar];
    }];
    
    NSError *error = nil;
    self.arrayAllItems = [self.fm contentsOfDirectoryAtURL:[NSURL fileURLWithPath:self.currentPath isDirectory:YES] includingPropertiesForKeys:[NSArray arrayWithObject:NSURLNameKey] options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
    
    if (error)
    {
        [InforDialog showErrorWithStatus:String(@"AccessDirectoryFailed") DisplayTime:PHFE_TIME_INFORDIALOG];
        self.arrayAllAvaliableItems = @[];
    }else
    {
        self.arrayAllAvaliableItems = [self filterContents:self.arrayAllItems];
        
        NSArray *tmpArrayCompressedFiles = [self getCompressedFiles:self.arrayAllAvaliableItems];
        
        if (bAutoExtract)
            [self ExtractCompressedFiles:tmpArrayCompressedFiles andDeleteOriginalFile:self.bDeleteOnceExtract];
        
        if (self.bHideCompressedFile)
        {
            NSMutableArray *tmpMutablArray = [[NSMutableArray alloc] initWithArray:self.arrayAllAvaliableItems];
            [tmpMutablArray removeObjectsInArray:tmpArrayCompressedFiles];
            self.arrayAllAvaliableItems = tmpMutablArray;
        }
    }
    
    self.gridViewContent.dataSource = self;
    self.gridViewContent.actionDelegate = self;
    [self.gridViewContent reloadData];
    
    [utils runAfter:0.1 andBlock:^(){
        [self HideLoadingBar];
    }];
}

- (NSArray *)filterContents:(NSArray *)arrayAllItems
{
    //    return arrayAllItems;
    NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] init];
    for (NSURL *tmpURL in self.arrayAllItems)
    {
        NSString *tmpString = [tmpURL lastPathComponent];
        //skip some special files
        //skip "__MACOSX"
        if ([tmpString isEqualToString:@"__MACOSX"]) continue;
        //skip ".DS_Store"
        if ([tmpString isEqualToString:@".DS_Store"]) continue;
        
        //skip "*.sqlite"
        NSString *tmpStrFileExtension = [PHFECommon getFileExtension:tmpString];
        tmpStrFileExtension = [tmpStrFileExtension uppercaseString];
        if ([tmpStrFileExtension isEqualToString:@"SQLITE"]) continue;
        if ([tmpStrFileExtension isEqualToString:@"SQLITE-SHM"]) continue;
        if ([tmpStrFileExtension isEqualToString:@"SQLITE-WAL"]) continue;
        
        //skip "*.plist" file
        if ([tmpStrFileExtension isEqualToString:@"PLIST"]) continue;
        
        [tmpMutableArray addObject:tmpString];
    }
    
    return tmpMutableArray;
}

- (NSArray *)getCompressedFiles:(NSArray *)arrayAllFiles
{
    NSMutableArray *tmpMutableArrayZippedFiles = [NSMutableArray new];
    
    for (NSString *tmpFileName in arrayAllFiles)
    {
        NSString *tmpFileExtension = [PHFECommon getFileExtension:tmpFileName];
        NSString *tmpFileType = [PHFECommon getTypeFromExtension:tmpFileExtension];
        tmpFileType = [tmpFileType uppercaseString];
        
        if ([tmpFileType isEqualToString:@"COMPRESSED"])
            [tmpMutableArrayZippedFiles addObject:tmpFileName];
    }
    
    return tmpMutableArrayZippedFiles;
}

- (void)ShowLoadingBar
{
    if (self.bShowLoadingBar)
        self.loadingVC.view.hidden = NO;
}

- (void)HideLoadingBar
{
    self.loadingVC.view.hidden = YES;
}

- (void)UpdateTheGridViewSize
{
    CGRect tmpRect;
    
    tmpRect.size = CGSizeMake(self.view.bounds.size.width - 2 * X_GRIDVIEW, self.view.bounds.size.height - 2 * Y_GRIDVIEW);
    
    if ([utils isIPhone])
        self.gridViewContent.itemSpacing = ITEM_SPACE_IPHONE;
    else
        self.gridViewContent.itemSpacing = ITEM_SPACE_IPAD;
    
    tmpRect.origin = CGPointMake(X_GRIDVIEW, Y_GRIDVIEW);
    
    self.gridViewContent.frame = tmpRect;
    self.gridViewContent.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

- (void)ExtractCompressedFiles:(NSArray *)arrayAllFiles andDeleteOriginalFile:(BOOL)flag
{
    if (self.bIsProcessingCompressedFiles) return;
    
    self.bIsProcessingCompressedFiles = YES;
    
    if (arrayAllFiles.count <= 0)
    {
        self.bIsProcessingCompressedFiles = NO;
        return;
    }
    
    [utils runInBackgroundThread:^(){
        
        __block MBProgressHUD *hud = nil;
    
        [utils runInMainThread:^(){
            hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.delegate = self;
            hud.labelText = String(@"Extracting");
        }];
        
        NSFileManager *fm = [NSFileManager defaultManager];
        
        for (NSString *tmpStrFileName in arrayAllFiles)
        {
            //process zip file
            ZipArchive *tmpZipArchive = [[ZipArchive alloc] init];
            
            NSString *tmpStrFilePath = [NSString stringWithFormat:@"%@/%@", self.currentPath, tmpStrFileName];
            if ([tmpZipArchive UnzipOpenFile:tmpStrFilePath])
            {
                NSString *tmpFileNameWithoutExtension = [[PHFECommon getFileNameWithoutExtension:tmpStrFilePath] copy];
                
                NSMutableArray *tmpArray = [[tmpFileNameWithoutExtension componentsSeparatedByString:@"/"] mutableCopy];
                [tmpArray removeLastObject];
                
                NSString *tmpStrParentDirectory = [tmpArray componentsJoinedByString:@"/"];
                
                BOOL ret = [tmpZipArchive UnzipFileTo:tmpStrParentDirectory overWrite:YES];
                
                [tmpZipArchive UnzipCloseFile];
                
                if (!ret)
                {
                    [utils runInMainThread:^(){
                        [InforDialog showInforWithStatus:String(@"UnableToExtractFile") DisplayTime:PHFE_TIME_INFORDIALOG];
                    }];
                }else
                {
                    if (flag)
                    {
                        NSError *error = nil;
                        [fm removeItemAtPath:tmpStrFilePath error:&error];
                        if (error)
                        {
                            [utils runInMainThread:^(){
                                [InforDialog showInforWithStatus:String(@"UnableToDeleteFile") DisplayTime:PHFE_TIME_INFORDIALOG];
                            }];
                        }
                    }
                }
            }else
            {
                [utils runInMainThread:^(){
                    [InforDialog showInforWithStatus:[NSString stringWithFormat:@"%@\n%@", String(@"UntableToOpenFile"), tmpStrFileName] DisplayTime:PHFE_TIME_INFORDIALOG];
                }];
            }
        }
        
        self.bIsProcessingCompressedFiles = NO;
        
        [utils runInMainThread:^(){
            [hud hide:YES];
            
            [self DirectoryContentChanged:NO];
        }];
    }];
}

- (void)ProcessDirectory:(NSURL *)url
{
    if (self.gridViewContent.editing) return;
}

- (void)ProcessFile:(NSURL *)url
{
    if (self.gridViewContent.editing) return;
    
    NSString *tmpFileName = [url lastPathComponent];
    NSString *tmpFileFullPath = [self.currentPath stringByAppendingFormat:@"/%@", tmpFileName];
    
    //based on the type of the file, use other classes to open the file
    NSString *tmpExtension = [[tmpFileName componentsSeparatedByString:@"."] lastObject];
    NSString *tmpFileType = [PHFECommon getTypeFromExtension:tmpExtension];
    tmpFileType = [tmpFileType uppercaseString];
    
    if ([tmpFileType isEqualToString:@"COMPRESSED"])
    {
        //this should not happen, because the compressed file will be extracted and deleted when the view first appear. but in case
        //process zip file
        
        __block MBProgressHUD *hud = nil;
        
        [utils runInBackgroundThread:^(){
            [utils runInMainThread:^(){
                hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                hud.labelText = String(@"Extracting");
                hud.delegate = self;
            }];
            ZipArchive *tmpZipArchive = [[ZipArchive alloc] init];
            if ([tmpZipArchive UnzipOpenFile:tmpFileFullPath])
            {
                NSString *tmpFileNameWithoutExtension = [[PHFECommon getFileNameWithoutExtension:tmpFileFullPath] copy];
                
                NSMutableArray *tmpArray = [[tmpFileNameWithoutExtension componentsSeparatedByString:@"/"] mutableCopy];
                [tmpArray removeLastObject];
                
                NSString *tmpStrParentDirectory = [tmpArray componentsJoinedByString:@"/"];
                
                BOOL ret = [tmpZipArchive UnzipFileTo:tmpStrParentDirectory overWrite:YES];
                
                if (ret == NO)
                {
                    [tmpZipArchive UnzipCloseFile];
                
                    [utils runInMainThread:^(){
                        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error"]];
                        hud.mode = MBProgressHUDModeCustomView;
                        hud.labelText = String(@"ExtractingFailed");

                        [utils runAfter:2 andBlock:^(){
                            [hud hide:YES];
                        }];
                    }];
                    return;
                }else
                {
                    [utils runInMainThread:^(){
                        hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"success"]];
                        hud.mode = MBProgressHUDModeCustomView;
                        hud.labelText = String(@"ExtractingComplete");
                        
                        [utils runAfter:2 andBlock:^(){
                            [hud hide:YES];
                        }];
                    }];
                }
            }else
            {
                [utils runInMainThread:^(){
                    hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"error"]];
                    hud.mode = MBProgressHUDModeCustomView;
                    hud.labelText = [NSString stringWithFormat:@"%@\n%@", String(@"ExtractingFailed"), String(@"UnableToOpenFile")];
                    
                    [utils runAfter:2 andBlock:^(){
                        [hud hide:YES];
                    }];
                }];
            }
        }];
        
    }else if ([tmpFileType isEqualToString:@"PDF"] || [tmpFileType isEqualToString:@"VIDEO"] || [tmpFileType isEqualToString:@"IMAGE"] || [tmpFileType isEqualToString:@"WEB"] || [tmpFileType isEqualToString:@"SPREADSHEET"] || [tmpFileType isEqualToString:@"WORD"] || [tmpFileType isEqualToString:@"POWERPOINT"] || [tmpFileType isEqualToString:@"RSS"] || [tmpFileType isEqualToString:@"XML"])
    {
        UIViewController *tmpViewController = nil;
        NSString *path = [self.currentPath stringByAppendingFormat:@"/%@", url.lastPathComponent];
        
        if ([tmpFileType isEqualToString:@"PDF"])
        {
            //process PDF file
            NSString *phrase = nil;
            ReaderDocument *pdf = [ReaderDocument withDocumentFilePath:path password:phrase];
            if (pdf != nil)
            {
                tmpViewController = [[ReaderViewController alloc] initWithReaderDocument:pdf andPath:(NSString *)path];
                ((ReaderViewController *)tmpViewController).pdfDelegate = self;
                [((ReaderViewController *)tmpViewController) setDelegate:self];
            }
        }else if ([tmpFileType isEqualToString:@"VIDEO"])
        {
            tmpViewController = [[MediaPlayerViewController alloc] initWithFrame:self.view.bounds];
            [(MediaPlayerViewController *)tmpViewController playMediaAt:path];
        }else if ([tmpFileType isEqualToString:@"IMAGE"])
        {
            tmpViewController = [[PhotoBrowserViewController alloc] initWithImagePath:self.currentPath withName:tmpFileFullPath.lastPathComponent];
        }
        else if ([tmpFileType isEqualToString:@"WEB"])
        {
            tmpViewController = [[WebContentViewController alloc] init];
            tmpViewController.view.frame = self.view.bounds;
            [(WebContentViewController *)tmpViewController showWebResourceAt:path];
        }
        else if ([tmpFileType isEqualToString:@"SPREADSHEET"] || [tmpFileType isEqualToString:@"WORD"] || [tmpFileType isEqualToString:@"POWERPOINT"])
        {
            tmpViewController = [[ExcelViewerViewController alloc] initWithFrame:self.view.bounds];
            [(ExcelViewerViewController *)tmpViewController ShowContent:path];
        }else if ([tmpFileType isEqualToString:@"RSS"] || [tmpFileType isEqualToString:@"XML"])
        {
            tmpViewController = [[ExcelViewerViewController alloc] initWithFrame:self.view.bounds];
            [(ExcelViewerViewController *)tmpViewController ShowContent:path];

        }
        if (tmpViewController)
        {
            tmpFileName = [PHFECommon getFileNameWithoutExtension:tmpFileName];
            tmpViewController.navigationItem.title = tmpFileName;
            [self.navigationController pushViewController:tmpViewController animated:YES];
        }else
        {
            [InforDialog showErrorWithStatus:[NSString stringWithFormat:@"%@\n%@", String(@"UnableToOpenFile"), tmpFileName] DisplayTime:PHFE_TIME_INFORDIALOG];
        }
    }else
    {
        [InforDialog showErrorWithStatus:[NSString stringWithFormat:@"%@\n%@", String(@"UnknownFileType"), tmpFileName] DisplayTime:PHFE_TIME_INFORDIALOG];
    }
}

- (void)TapItem:(int)position
{
    [self GMGridView:self.gridViewContent didTapOnItemAtIndex:position];
}

#pragma mark - ItemGMGridViewCellDelegate

- (void)ItemGMGridViewCellLongPressed:(id)cell
{
    if (self.gridViewContent.editing) return;
    
    self.gridViewContent.editing = !self.gridViewContent.editing;
    
    NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] initWithArray:self.navigationItem.rightBarButtonItems];
    [tmpMutableArray addObject:self.barBtnCancel];
    self.navigationItem.rightBarButtonItems = tmpMutableArray;
}

#pragma mark - PDFContentViewerDelegate

- (void)PDFContentViewerDoubleTapped:(ReaderViewController *)vc
{
    if ([self.delegate respondsToSelector:@selector(PHFileExplorerContentDoubleTapped:)])
        [self.delegate PHFileExplorerContentDoubleTapped:self];
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{    
    if (alertView.tag == PHFE_TAG_ALERTVIEW_DELETE)
    {
        if (buttonIndex == 0)
        {
            NSFileManager *fm = [NSFileManager defaultManager];
            
            if ([alertView isKindOfClass:[PHAlertViewWithObject class]])
            {
                NSString *tmpFileNameFullPath = (NSString *)((PHAlertViewWithObject *)alertView).object;
                NSLog(@"tmpFileNameFullPath: %@", tmpFileNameFullPath);
                
                BOOL isDir = NO;
                NSError *error = nil;
                
                if ([fm fileExistsAtPath:tmpFileNameFullPath isDirectory:&isDir])
                {
                    if (isDir)
                    {
                        //delete the whole directory
                        [fm removeItemAtPath:tmpFileNameFullPath error:&error];
                        if (error)
                            [InforDialog showErrorWithStatus:String(@"UnableToDeleteDirectory") DisplayTime:PHFE_TIME_INFORDIALOG];
                        else
                            [InforDialog showSuccessWithStatus:String(@"DeleteDirectoryComplete") DisplayTime:PHFE_TIME_INFORDIALOG];
                    }else
                    {
                        //delete the single file
                        [fm removeItemAtPath:tmpFileNameFullPath error:&error];
                        if (error)
                            [InforDialog showErrorWithStatus:String(@"UnableToDeleteFile") DisplayTime:PHFE_TIME_INFORDIALOG];
                        else
                            [InforDialog showSuccessWithStatus:String(@"DeleteFileComplete") DisplayTime:PHFE_TIME_INFORDIALOG];
                    }
                }else
                {
                    [InforDialog showErrorWithStatus:[NSString stringWithFormat:@"%@\n%@", String(@"UnexpectedError"), String(@"FileNotExist")] DisplayTime:PHFE_TIME_INFORDIALOG];
                }
            }
        }
    }
}

#pragma mark - MBProgressHUDDelegate

- (void)hudWasHidden:(MBProgressHUD *)hud
{
    [hud removeFromSuperview];
    hud = nil;
}

@end
