//
//  BasicFileExplorerViewController.h
//  Sidekick
//
//  Created by Ricol Wang on 28/06/13.
//
//

#import <UIKit/UIKit.h>
#import "GMGridView.h"
#import "ItemGMGridViewCell.h"
#import "GMGridViewLayoutStrategies.h"
#import "PHFileExplorerDelegate.h"
#import "PHFileExplorerDataSource.h"

@interface BasicFileExplorerViewController : UIViewController

@property (weak) id <PHFileExplorerDelegate> delegate;
@property (weak) id <PHFileExplorerDataSource> datasoure;
@property (weak) IBOutlet UIImageView *imageViewBackground;
@property (weak) IBOutlet GMGridView *gridViewContent;

@property (strong) NSString *currentPath;
@property (strong) NSArray *arrayAllAvaliableItems;
@property BOOL bAutoExtract;
@property BOOL bDeleteOnceExtract;
@property BOOL bShowLoadingBar;
@property BOOL bPDFPreview;
@property BOOL bImagePreview;
@property BOOL bHideCompressedFile;
@property (nonatomic) int itemsIPadLandscape;
@property (nonatomic) int itemsIPadPortrait;
@property (nonatomic) int itemsIPhoneLandscape;
@property (nonatomic) int itemsIPhonePortrait;

- (void)InitializeWithPath:(NSString *)path andRowForIpadLandscape:(int)rowForIPadLandscape andRowForIPadPortrait:(int)rowForIPadPortrait andRowForIPhoneLandscape:(int)rowForIPhoneLandscape andRowForIPhonePortrait:(int)rowForIPhonePortrait;
- (void)ConfigureCell:(ItemGMGridViewCell *)cell withImage:(UIImage *)image withTitle:(NSString *)title andFullPath:(NSString *)fullpath;
- (void)ProcessDirectory:(NSURL *)url;
- (void)ProcessFile:(NSURL *)url;
- (NSArray *)filterContents:(NSArray *)arrayAllItems;
- (void)TapItem:(int)position;

@end
