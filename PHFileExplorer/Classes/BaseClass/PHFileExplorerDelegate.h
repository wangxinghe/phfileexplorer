//
//  PHFileExplorerDelegate.h
//  PHFileExplorer
//
//  Created by Ricol Wang on 14/08/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BasicFileExplorerViewController;

@protocol PHFileExplorerDelegate <NSObject>

@optional

- (void)PHFileExplorerContentDoubleTapped:(BasicFileExplorerViewController *)vc;

@end
