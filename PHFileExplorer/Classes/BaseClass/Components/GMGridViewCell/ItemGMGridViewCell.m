//
//  ItemGMGridViewCell.m
//  Sidekick
//
//  Created by Ricol Wang on 6/05/13.
//
//

#import "ItemGMGridViewCell.h"
#import <PHCommon/utils.h>
#import "PHFEHeader.h"

//#define _DEBUG

#define NUMBER 5

@interface ItemGMGridViewCell ()

@property (strong) UIImageView *imageView;
@property (strong) UILabel *title;
@property (strong) UIImage *image;
@property (strong) NSString *strTitle;
@property (strong) UILongPressGestureRecognizer *longPressGesture;

@end

@implementation ItemGMGridViewCell

- (id)initWithImage:(UIImage *)image andTitle:(NSString *)title
{
    self = [super init];
    
    if (self)
    {
        self.image = image;
        self.strTitle = title;
        self.deleteButtonIcon = [UIImage imageNamed:@"close_x"];
#ifdef _DEBUG
        self.backgroundColor = [utils randomColor];
#else
        self.backgroundColor = [UIColor clearColor];
#endif
        
        if (!self.longPressGesture)
        {
            self.longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
            [self addGestureRecognizer:self.longPressGesture];
        }
    }
    
    return self;
}

- (void)handleLongPressGesture:(UILongPressGestureRecognizer *)longPressGesture
{
    if (longPressGesture.state != UIGestureRecognizerStateBegan) return;
    
    [self.delegate ItemGMGridViewCellLongPressed:self];
}

- (void)NewTitle:(NSString *)title
{
    self.strTitle = title;
    [self setNeedsDisplay];
}

- (void)NewImage:(UIImage *)image
{
    self.image = image;
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [super drawRect:rect];
    
    if (!self.contentView)
    {
        self.contentView = [[UIView alloc] initWithFrame:self.bounds];
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
#ifdef _DEBUG
        self.contentView.backgroundColor = [utils randomColor];
#else
        self.contentView.backgroundColor = [UIColor clearColor];
#endif
        
        //set accessibility
        self.contentView.isAccessibilityElement = YES;
        self.contentView.accessibilityLabel = [NSString stringWithFormat:@"%@", self.strTitle];
        self.contentView.accessibilityHint = [NSString stringWithFormat:@"Double tap to open the %@", self.strTitle];
    }
    
    if (!self.imageView)
    {
        self.imageView = [[UIImageView alloc] initWithImage:self.image];
        CGRect tmpRect = self.contentView.bounds;
        tmpRect.size.height -= tmpRect.size.height / NUMBER;
        self.imageView.frame = tmpRect;
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
#ifdef _DEBUG
        self.imageView.backgroundColor = [utils randomColor];
#else
        self.imageView.backgroundColor = [UIColor clearColor];
#endif
        self.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.contentView addSubview:self.imageView];
    }
    
    self.imageView.image = self.image;
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    if (!self.title)
    {
        CGRect tmpRect = self.contentView.bounds;
        tmpRect.size.height /= NUMBER;
        tmpRect.origin.y = self.contentView.bounds.size.height - tmpRect.size.height;
        self.title = [[UILabel alloc] initWithFrame:tmpRect];
        self.title.numberOfLines = 3;
#ifdef _DEBUG
        self.title.backgroundColor = [utils randomColor];
#else
        self.title.backgroundColor = [UIColor clearColor];
#endif
        self.title.text = self.strTitle;
        self.title.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
        self.title.textAlignment = NSTextAlignmentCenter;
        self.title.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
        [self.contentView addSubview:self.title];
    }
    
    self.title.text = self.strTitle;
}

#pragma mark - IUpdateGUIWithPreview

- (void)updateGuiWithPreview:(UIImage *)preview withFilepath:(NSString *)filepath
{
    if ([self.filepath isEqualToString:filepath])
        [self NewImage:preview];
}

- (CGSize)getPreviewSize
{
    return self.image.size;
}

@end
