//
//  ItemGMGridViewCell.h
//  Sidekick
//
//  Created by Ricol Wang on 6/05/13.
//
//

#import "GMGridViewCell.h"
#import "ItemGMGridViewCellDelegate.h"
#import "IUpdateGUIWithPreview.h"

@interface ItemGMGridViewCell : GMGridViewCell <IUpdateGUIWithPreview>

@property (copy) NSString *filepath;
@property (weak) id <ItemGMGridViewCellDelegate> delegate;

- (id)initWithImage:(UIImage *)image andTitle:(NSString *)title;
- (void)NewTitle:(NSString *)title;
- (void)NewImage:(UIImage *)image;

@end
