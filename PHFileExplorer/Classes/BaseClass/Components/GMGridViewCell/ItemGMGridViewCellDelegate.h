//
//  ItemGMGridViewCellDelegate.h
//  PHFileExplorer
//
//  Created by Ricol Wang on 11/08/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ItemGMGridViewCell;

@protocol ItemGMGridViewCellDelegate <NSObject>

- (void)ItemGMGridViewCellLongPressed:(ItemGMGridViewCell *)cell;

@end
