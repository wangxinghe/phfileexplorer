//
//  DefaultViewerController.h
//  GMGridViewExample
//
//  Created by Philip Cookson on 11/07/12.
//  Copyright (c) 2012 GMoledina.ca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasicContentViewController.h"

@interface DefaultViewerController : BasicContentViewController

- (void)showWebResourceAt:(NSString *)resourcePath;
- (void)closeView;

@end
