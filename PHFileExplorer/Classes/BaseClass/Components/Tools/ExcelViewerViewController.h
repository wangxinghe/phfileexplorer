//
//  ExcelViewerViewController.h
//  Sidekick
//
//  Created by Ricol Wang on 12/04/13.
//
//

#import "BasicContentViewController.h"

@interface ExcelViewerViewController : BasicContentViewController

- (id)initWithFrame:(CGRect)frame;
- (void)ShowContent:(NSString *)Path;
- (void)CloseView;

@end
