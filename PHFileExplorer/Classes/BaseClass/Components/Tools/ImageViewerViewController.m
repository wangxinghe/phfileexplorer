//
//  ImageViewerViewController.m
//  GMGridViewExample
//
//  Created by Philip Cookson on 11/07/12.
//  Copyright (c) 2012 GMoledina.ca. All rights reserved.
//

#import "ImageViewerViewController.h"
#import "PHFECommon.h"
#import "PHNavigateDelegate.h"

#define ZOOM_VIEW_TAG 100
#define ZOOM_STEP 1.5
#define TIME_INTERVAL 0.4

@interface ImageViewerViewController () <PHNavigateDelegate>

@property (strong) ZoomingScrollView *imageScrollView;
@property (strong) NSArray *array_Files;
@property (strong) NSString *directory;
@property int currentIndex;

- (void)MoveFromCentreToRight:(UIView *)view;
- (void)MoveFromCentreToLeft:(UIView *)view;

- (NSArray *)filterImages:(NSArray *)array;

@end

@implementation ImageViewerViewController

- (id)initWithFrame:(CGRect)frame andDirectory:(NSString *)directory
{
    self = [super init];
    if (self)
    {
        self.view.frame = frame;
        self.directory = directory;
        self.array_Files = [PHFECommon getAllFilesUnderDirectory:directory andSubDirectory:NO];
        self.array_Files = [self filterImages:self.array_Files];

        self.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.btnInfor.hidden = NO;
    self.btnEmail.hidden = NO;
    self.btnPrint.hidden = NO;
    
    if ([utils isIOS7])
    {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
    }
}

- (NSArray *)filterImages:(NSArray *)array
{
    NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] init];
    
    if (array)
    {
        for (NSString *tmpString in array)
        {
            NSString *tmpExtension = [PHFECommon getFileExtension:tmpString];
            tmpExtension = [tmpExtension lowercaseString];
            NSString *tmpFileType = [PHFECommon getTypeFromExtension:tmpExtension];
            tmpFileType = [tmpFileType lowercaseString];
            if ([tmpFileType isEqualToString:@"image"])
                [tmpMutableArray addObject:tmpString];
        }
    }
    
    return tmpMutableArray;
}

- (void)MoveFromCentreToRight:(UIView *)view
{
    CGRect tmpRect = view.frame;
    tmpRect.origin.x = 1024 * 1.5;
    [UIView animateWithDuration:TIME_INTERVAL animations:^(){
        view.frame = tmpRect;
    }completion:^(BOOL finished){
        [view removeFromSuperview];
    }];
}

- (void)MoveFromCentreToLeft:(UIView *)view
{
    CGRect tmpRect = view.frame;
    tmpRect.origin.x = -1024 * 1.5;
    [UIView animateWithDuration:TIME_INTERVAL animations:^(){
        view.frame = tmpRect;
    }completion:^(BOOL finished){
        [view removeFromSuperview];
    }];
}

- (void)MoveLeftToCentre:(UIView *)view
{
    CGRect tmpRectOriginal = view.frame;
    CGRect tmpRect = view.frame;
    tmpRect.origin.x = -1024 * 1.5;
    view.frame = tmpRect;
    view.hidden = NO;
    [UIView animateWithDuration:TIME_INTERVAL animations:^(){
        view.frame = tmpRectOriginal;
    } completion:^(BOOL finished){
        view.hidden = NO;
    }];
}

- (void)MoveRightToCentre:(UIView *)view
{
    CGRect tmpRectOriginal = view.frame;
    CGRect tmpRect = view.frame;
    tmpRect.origin.x = 1024 * 1.5;
    view.frame = tmpRect;
    view.hidden = NO;
    [UIView animateWithDuration:TIME_INTERVAL animations:^(){
        view.frame = tmpRectOriginal;
    } completion:^(BOOL finished){
        view.hidden = NO;
    }];
}

- (void) showImageAt:(NSString *)imagePath
{
    self.imageScrollView = [[ZoomingScrollView alloc] initWithFrame:self.view.bounds andDelegate:self];
    self.imageScrollView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.imageScrollView.backgroundColor = [UIColor clearColor];
    self.imageScrollView.userInteractionEnabled = YES;
    [self.view addSubview:self.imageScrollView];
    
    self.filename = imagePath;
    NSString *tmpFileName = [PHFECommon getFileNameFromFullPath:imagePath];
    tmpFileName = [PHFECommon stringWithoutSubStringAtTheFront:tmpFileName andSubString:@"b_" andCaseSensitive:NO];
    tmpFileName = [PHFECommon getFileNameWithoutExtension:tmpFileName];
    self.navigationItem.title = tmpFileName;
    self.currentIndex = [self.array_Files indexOfObject:imagePath];
    NSLog(@"self.array_Files: \n%@", self.array_Files);
    NSLog(@"self.filename: %@", self.filename);
    NSLog(@"self.currentIndex: %d", self.currentIndex);
    [self.imageScrollView displayImageAtPath:[NSURL fileURLWithPath:imagePath]];
}

- (void) closeView
{
    [self.imageScrollView removeFromSuperview];
    self.imageScrollView = nil;
}

#pragma mark - PHNavigateDelegate

- (void)ShowNextItem
{
    ++self.currentIndex;
    if (self.currentIndex > self.array_Files.count - 1)
        self.currentIndex = 0;
    
    [self MoveFromCentreToLeft:self.imageScrollView];
    
    self.imageScrollView = [[ZoomingScrollView alloc] initWithFrame:self.view.bounds andDelegate:self];
    self.imageScrollView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.imageScrollView.backgroundColor = [UIColor clearColor];
    self.imageScrollView.userInteractionEnabled = YES;
    self.imageScrollView.hidden = YES;
    [self.view addSubview:self.imageScrollView];
    
    NSString *imagePath = self.array_Files[self.currentIndex];
    self.filename = imagePath;
    NSString *tmpFileName = [PHFECommon getFileNameFromFullPath:imagePath];
    tmpFileName = [PHFECommon stringWithoutSubStringAtTheFront:tmpFileName andSubString:@"b_" andCaseSensitive:NO];
    tmpFileName = [PHFECommon getFileNameWithoutExtension:tmpFileName];
    self.navigationItem.title = tmpFileName;
    self.currentIndex = [self.array_Files indexOfObject:imagePath];
    NSLog(@"self.array_Files: \n%@", self.array_Files);
    NSLog(@"self.filename: %@", self.filename);
    NSLog(@"self.currentIndex: %d", self.currentIndex);
    [self.imageScrollView displayImageAtPath:[NSURL fileURLWithPath:imagePath]];
    
    [self MoveRightToCentre:self.imageScrollView];
}

- (void)ShowPreviousItem
{
    --self.currentIndex;
    if (self.currentIndex < 0)
        self.currentIndex = self.array_Files.count - 1;
    
    [self MoveFromCentreToRight:self.imageScrollView];
    
    self.imageScrollView = [[ZoomingScrollView alloc] initWithFrame:self.view.bounds andDelegate:self];
    self.imageScrollView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.imageScrollView.backgroundColor = [UIColor clearColor];
    self.imageScrollView.userInteractionEnabled = YES;
    self.imageScrollView.hidden = YES;
    [self.view addSubview:self.imageScrollView];
    
    NSString *imagePath = self.array_Files[self.currentIndex];
    self.filename = imagePath;
    NSString *tmpFileName = [PHFECommon getFileNameFromFullPath:imagePath];
    tmpFileName = [PHFECommon stringWithoutSubStringAtTheFront:tmpFileName andSubString:@"b_" andCaseSensitive:NO];
    tmpFileName = [PHFECommon getFileNameWithoutExtension:tmpFileName];
    self.navigationItem.title = tmpFileName;
    self.currentIndex = [self.array_Files indexOfObject:imagePath];
    NSLog(@"self.array_Files: \n%@", self.array_Files);
    NSLog(@"self.filename: %@", self.filename);
    NSLog(@"self.currentIndex: %d", self.currentIndex);
    [self.imageScrollView displayImageAtPath:[NSURL fileURLWithPath:imagePath]];
    
    [self MoveLeftToCentre:self.imageScrollView];
}

@end
