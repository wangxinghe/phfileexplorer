//
//  WebContentViewController.h
//  Sidekick
//
//  Created by Ricol Wang on 12/04/13.
//
//

#import "BasicContentViewController.h"

@interface WebContentViewController : BasicContentViewController

- (void)showWebResourceAt:(NSString *)resourcePath;
- (void)closeView;
- (void)loadWithLocalContent:(NSString *)contentPath;

@end
