//
//  MediaPlayerViewController.m
//  GMGridViewExample
//
//  Created by Philip Cookson on 10/07/12.
//  Copyright (c) 2012 GMoledina.ca. All rights reserved.
//

#import "MediaPlayerViewController.h"
#import "PHFECommon.h"

@interface MediaPlayerViewController ()

@property (strong) MPMoviePlayerController * mediaPlayer;

@end

@implementation MediaPlayerViewController

- (id)initWithFrame:(CGRect)frame
{
    self = [super init];
    if (self)
    {
        self.bVideoIsFullScreen = NO;
        self.view.frame = frame;
        self.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
        
        self.mediaPlayer = [[MPMoviePlayerController alloc] init];
        [self.mediaPlayer setControlStyle:MPMovieControlStyleEmbedded];

        [self.mediaPlayer.view setFrame:self.view.bounds];
        self.mediaPlayer.view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;

        [self.view addSubview:self.mediaPlayer.view];
        [self.view bringSubviewToFront:self.mediaPlayer.view];
        self.view.userInteractionEnabled = YES;
        self.mediaPlayer.view.backgroundColor = [UIColor clearColor];
        self.mediaPlayer.shouldAutoplay = YES;
        
        [self.mediaPlayer prepareToPlay];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MoviePlayerWillEnterFullScreen:) name:MPMoviePlayerWillEnterFullscreenNotification object:self.mediaPlayer];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MoviePlayerWillExitFullScreen:) name:MPMoviePlayerWillExitFullscreenNotification object:self.mediaPlayer];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MoviePlayerDidEnterFullScreen:) name:MPMoviePlayerDidEnterFullscreenNotification object:self.mediaPlayer];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(MoviePlayerDidExitFullScreen:) name:MPMoviePlayerDidExitFullscreenNotification object:self.mediaPlayer];
    }
    return self;
}

- (void) playMediaAt:(NSString *)mediaPath
{
    @try {
        self.filename = mediaPath;
        [self.mediaPlayer setContentURL:[NSURL fileURLWithPath:mediaPath]];
        [self.mediaPlayer play];
    }
    @catch (NSException *exception) {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Unable to play this file!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
    }
    @finally {
        ;
    }

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (self.bVideoIsFullScreen) return;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self.mediaPlayer stop];
    self.mediaPlayer = nil;
}

- (void)MoviePlayerWillEnterFullScreen:(NSNotification *)notification
{
    self.bVideoIsFullScreen = YES;
}

- (void)MoviePlayerDidEnterFullScreen:(NSNotification *)notification
{
    self.bVideoIsFullScreen = YES;
}

- (void)MoviePlayerWillExitFullScreen:(NSNotification *)notification
{
    self.bVideoIsFullScreen = NO;
}

- (void)MoviePlayerDidExitFullScreen:(NSNotification *)notification
{
    self.bVideoIsFullScreen = NO;
}

@end
