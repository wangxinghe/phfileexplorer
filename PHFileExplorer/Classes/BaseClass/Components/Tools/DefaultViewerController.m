//
//  DefaultViewerController.m
//  GMGridViewExample
//
//  Created by Philip Cookson on 11/07/12.
//  Copyright (c) 2012 GMoledina.ca. All rights reserved.
//

#import "DefaultViewerController.h"

@interface DefaultViewerController ()

@end

@implementation DefaultViewerController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.contentWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.contentWebView.delegate = self;
    self.contentWebView.backgroundColor = [UIColor clearColor];
    self.contentWebView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.contentWebView];
    
    self.activitityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activitityIndicator.color = [UIColor blueColor];
    
    NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] initWithArray:self.navigationItem.rightBarButtonItems];
    UIBarButtonItem *tmpBar = [[UIBarButtonItem alloc] initWithCustomView:self.activitityIndicator];
    [tmpMutableArray addObject:tmpBar];
    self.navigationItem.rightBarButtonItems = tmpMutableArray;
    //    self.activitityIndicator.frame = CGRectMake(self.view.bounds.size.width / 2 - 25, self.view.bounds.size.height - ACTIVITY_INDICATOR_BOTTOM_MARGIN, 50, 50);
    //    self.activitityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    self.activitityIndicator.hidesWhenStopped = YES;
    //    [self.view addSubview:self.activitityIndicator];
    
    self.btnInfor.hidden = NO;
    self.btnEmail.hidden = NO;
}

- (void)showWebResourceAt:(NSString *)resourcePath
{
    self.filename = resourcePath;
    [self.contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithContentsOfFile:resourcePath encoding:NSUTF8StringEncoding error:nil]]]];
}

- (void)closeView
{
    [self.contentWebView loadHTMLString:@"HELLO" baseURL:nil];
    [self.contentWebView removeFromSuperview];
    self.contentWebView = nil;
}

@end
