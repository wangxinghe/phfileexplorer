//
//  ExcelViewerViewController.m
//  Sidekick
//
//  Created by Ricol Wang on 12/04/13.
//
//

#import "ExcelViewerViewController.h"

@interface ExcelViewerViewController ()

@end

@implementation ExcelViewerViewController

- (id)initWithFrame:(CGRect)frame
{
    self = [super init];
    if (self)
    {
        self.view.frame = frame;
        self.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.contentWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.contentWebView.delegate = self;
    self.contentWebView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.contentWebView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.contentWebView];
    
    self.activitityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activitityIndicator.color = [UIColor blueColor];
    
    NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] initWithArray:self.navigationItem.rightBarButtonItems];
    UIBarButtonItem *tmpBar = [[UIBarButtonItem alloc] initWithCustomView:self.activitityIndicator];
    [tmpMutableArray addObject:tmpBar];
    self.navigationItem.rightBarButtonItems = tmpMutableArray;
    //    self.activitityIndicator.frame = CGRectMake(self.view.bounds.size.width / 2 - 25, self.view.bounds.size.height - ACTIVITY_INDICATOR_BOTTOM_MARGIN, 50, 50);
    //    self.activitityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    self.activitityIndicator.hidesWhenStopped = YES;
    //    [self.view addSubview:self.activitityIndicator];
    
    self.btnInfor.hidden = NO;
    self.btnEmail.hidden = NO;
}

- (void)ShowContent:(NSString *)Path
{
    self.filename = Path;
    [self.contentWebView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:Path]]];
}

- (void)CloseView
{
    [self.contentWebView loadHTMLString:@"HELLO" baseURL:nil];
    [self.contentWebView removeFromSuperview];
    self.contentWebView = nil;
}
@end
