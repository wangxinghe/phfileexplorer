//
//  ZoomingScrollView.h
//  MWPhotoBrowser
//
//  Created by Michael Waterfall on 14/10/2010.
//  Copyright 2010 d3i. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIImageViewTap.h"
#import "UIViewTap.h"
#import "PHNavigateDelegate.h"
//#import "ImageViewerViewController.h"

@class MWPhotoBrowser;

@interface ZoomingScrollView : UIScrollView <UIScrollViewDelegate, UIImageViewTapDelegate, UIViewTapDelegate> {
	
	// Browser
//	ImageViewerViewController *photoBrowser;
	
	// State
	NSUInteger index;
	

	
}

// Views
@property (strong) UIViewTap *tapView; // for background taps
@property (strong) UIImageViewTap *photoImageView;
@property (strong) UIActivityIndicatorView *spinner;
@property (weak) id <PHNavigateDelegate>ParentDelegate;

// Properties
@property NSUInteger index;
//@property (nonatomic, assign) ImageViewerViewController *photoBrowser;

// Methods
- (void)displayImageAtPath:(NSURL *)imagePath;
- (void)displayImageFailure;
- (void)setMaxMinZoomScalesForCurrentBounds;
- (void)handleSingleTap:(CGPoint)touchPoint;
- (void)handleDoubleTap:(CGPoint)touchPoint;

- (id)initWithFrame:(CGRect)frame andDelegate:(id <PHNavigateDelegate>)delegate;

@end
