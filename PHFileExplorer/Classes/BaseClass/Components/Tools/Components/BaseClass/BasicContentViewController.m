//
//  BasicContentViewController.m
//  Sidekick
//
//  Created by Ricol Wang on 12/04/13.
//
//

#import "BasicContentViewController.h"
#import "PHFECommon.h"
#import "PHFEHeader.h"
#import <MessageUI/MessageUI.h>
#import <InforDialog.h>

#define WIDTH_ICON 32
#define HEIGHT_ICON 32
#define GAP 12

@interface BasicContentViewController () <MFMailComposeViewControllerDelegate>

@property (strong) UIPrintInteractionController *printInteraction;

@end

#pragma mark - View Life Cycle

@implementation BasicContentViewController

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    CGSize tmpSize = CGSizeMake(467, 44);
//    self.toolBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tmpSize.width, tmpSize.height)];
//    self.toolBarView.backgroundColor = [UIColor clearColor];
//    
//    self.btnInfor = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.btnInfor setImage:[UIImage imageNamed:@"Internal-Header-Info-Button-White"] forState:UIControlStateNormal];
//    self.btnInfor.frame = CGRectMake(tmpSize.width - 1 * (WIDTH_ICON + GAP), (tmpSize.height - HEIGHT_ICON) / 2, WIDTH_ICON, HEIGHT_ICON);
//    [self.btnInfor addTarget:self action:@selector(GoInformation:) forControlEvents:UIControlEventTouchUpInside];
//    [self.toolBarView addSubview:self.btnInfor];
//    self.btnInfor.hidden = YES;
//    
//    self.btnEmail = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.btnEmail setImage:[UIImage imageNamed:@"Internal-Email-Button-White"] forState:UIControlStateNormal];
//    self.btnEmail.frame = CGRectMake(tmpSize.width - 2 * (WIDTH_ICON + GAP), (tmpSize.height - HEIGHT_ICON) / 2, WIDTH_ICON, HEIGHT_ICON);
//    [self.btnEmail addTarget:self action:@selector(GoEmail:) forControlEvents:UIControlEventTouchUpInside];
//    [self.toolBarView addSubview:self.btnEmail];
//    self.btnEmail.hidden = YES;
//
//    self.btnPrint = [UIButton buttonWithType:UIButtonTypeCustom];
//    [self.btnPrint setImage:[UIImage imageNamed:@"Internal-Print-Button"] forState:UIControlStateNormal];
//    self.btnPrint.frame = CGRectMake(tmpSize.width - 3 * (WIDTH_ICON + GAP), (tmpSize.height - HEIGHT_ICON) / 2, WIDTH_ICON, HEIGHT_ICON);
//    [self.btnPrint addTarget:self action:@selector(GoPrint:) forControlEvents:UIControlEventTouchUpInside];
//    [self.toolBarView addSubview:self.btnPrint];
//    self.btnPrint.hidden = YES;
    
    if ([utils isIOS7])
    {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
    }
    
#ifdef _DEBUG
    self.view.backgroundColor = [utils randomColor];
#endif
}

#pragma mark - Public Methods

- (void)GoBack:(id)Sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)GoInformation:(id)Sender
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    NSDictionary *tmpDictionary = [fm attributesOfItemAtPath:self.filename error:&error];
    NSString *tmpStr_Message = @"";
    if (error)
    {
        tmpStr_Message = [NSString stringWithFormat:@"Unable to get information of the file %@", [PHFECommon getFileNameFromFullPath:self.filename]];
    }else
    {
        tmpStr_Message = [NSString stringWithFormat:@"File Attribute: \nCreation Date: %@\nExtension Hidden: %@\nGroup Owner Account ID: %@\nGroup Owner Account Name: %@\nModification Date: %@\nOwner Account ID: %@\nOwner Account Name: %@\nPosix Permissions: %@\nProtection Key: %@\nReference Count: %@\nFile Size: %@\nSystem File Number: %@\nSystem Number: %@\nFile Type: %@", tmpDictionary[NSFileCreationDate], tmpDictionary[NSFileExtensionHidden], tmpDictionary[NSFileGroupOwnerAccountID], tmpDictionary[NSFileGroupOwnerAccountName], tmpDictionary[NSFileModificationDate], tmpDictionary[NSFileOwnerAccountID], tmpDictionary[NSFileOwnerAccountName], tmpDictionary[NSFilePosixPermissions], tmpDictionary[NSFileProtectionKey], tmpDictionary[NSFileReferenceCount], tmpDictionary[NSFileSize], tmpDictionary[NSFileSystemFileNumber], tmpDictionary[NSFileSystemNumber], tmpDictionary[NSFileType]];
    }
    
    [InforDialog showInforWithStatus:tmpStr_Message DisplayTime:PHFE_TIME_INFORDIALOG];
}

- (void)GoHome:(id)Sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)GoPrint:(id)Sender
{
    [self GoPrintWithFileName:self.filename andSender:Sender];
}

- (void)GoPrintWithFileName:(NSString *)filename andSender:(id)Sender
{
//    Class printInteractionController = NSClassFromString(@"UIPrintInteractionController");
//    
//	if ((printInteractionController != nil) && [printInteractionController isPrintingAvailable])
//	{
//		NSURL *fileURL = [NSURL fileURLWithPath:filename];
//        
//		self.printInteraction = [printInteractionController sharedPrintController];
//        
//		if ([printInteractionController canPrintURL:fileURL] == YES) // Check first
//		{
//			UIPrintInfo *printInfo = [NSClassFromString(@"UIPrintInfo") printInfo];
//            
//			printInfo.duplex = UIPrintInfoDuplexLongEdge;
//			printInfo.outputType = UIPrintInfoOutputGeneral;
//			printInfo.jobName = filename;
//            
//			self.printInteraction.printInfo = printInfo;
//			self.printInteraction.printingItem = fileURL;
//			self.printInteraction.showsPageRange = YES;
//            
//			if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
//			{
//                UIButton *tmpButton = Sender;
//                
//                if ([tmpButton isKindOfClass:[UIBarButtonItem class]])
//                {
//                    [self.printInteraction presentFromBarButtonItem:(UIBarButtonItem *)tmpButton animated:YES completionHandler:^(UIPrintInteractionController *pic, BOOL completed, NSError *error)
//                     {
//#ifdef _DEBUG
//                         if ((completed == NO) && (error != nil)) NSLog(@"%s %@", __FUNCTION__, error);
//#endif
//                     }];
//                }else
//                {
//                    [self.printInteraction presentFromRect:tmpButton.frame inView:self.toolBarView animated:YES completionHandler:^(UIPrintInteractionController *pic, BOOL completed, NSError *error)
//                     {
//#ifdef _DEBUG
//                         if ((completed == NO) && (error != nil)) NSLog(@"%s %@", __FUNCTION__, error);
//#endif
//                     }];
//                }
//			}
//			else // Presume UIUserInterfaceIdiomPhone
//			{
//				[self.printInteraction presentAnimated:YES completionHandler:
//                 ^(UIPrintInteractionController *pic, BOOL completed, NSError *error)
//                 {
//#ifdef _DEBUG
//                     if ((completed == NO) && (error != nil)) NSLog(@"%s %@", __FUNCTION__, error);
//#endif
//                 }
//                 ];
//			}
//		}else
//        {
//            UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"MESSAGE" message:@"Unable to print the file!" delegate:self cancelButtonTitle:DIALOG_MESSAGE_OK otherButtonTitles:nil];
//            [tmpAlertView show];
    
//        }
//	}
}

- (void)GoEmail:(id)Sender
{
    [self GoEmailWithFileName:self.filename];
}

- (void)GoEmailWithFileName:(NSString *)filename
{
    if ([MFMailComposeViewController canSendMail] == NO)
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"MESSAGE" message:@"Email requires a valid email account to be configured." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
        return;
    }
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    NSDictionary *tmpDictionary = [fm attributesOfItemAtPath:filename error:&error];
    if (error)
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"MESSAGE" message:@"Sending Email Failed!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
        return;
    }
	unsigned long long fileSize = [tmpDictionary[NSFileSize] unsignedLongLongValue];
    
	if (fileSize < (unsigned long long)15728640) // Check attachment size limit (15MB)
	{
        NSURL *fileURL = [NSURL fileURLWithPath:filename];
        
		NSData *attachment = [NSData dataWithContentsOfURL:fileURL options:(NSDataReadingMapped|NSDataReadingUncached) error:nil];
        
		if (attachment != nil) // Ensure that we have valid document file attachment data
		{
			MFMailComposeViewController *mailComposer = [MFMailComposeViewController new];
            
            NSString *tmpFileShortName = [PHFECommon getFileNameFromFullPath:filename];
			[mailComposer addAttachmentData:attachment mimeType:@"application/doc" fileName:tmpFileShortName];
            
			[mailComposer setSubject:[PHFECommon getFileNameFromFullPath:filename]]; // Use the document file name for the subject
            
			mailComposer.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
			mailComposer.modalPresentationStyle = UIModalPresentationPageSheet;
            mailComposer.navigationBar.barStyle = UIBarStyleDefault;
            
			mailComposer.mailComposeDelegate = self; // Set the delegate
            
			[self presentModalViewController:mailComposer animated:YES];
		}else
        {
            UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"MESSAGE" message:@"Unable to attach the file!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [tmpAlertView show];
        }
	}else
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"MESSAGE" message:@"Unable to email the file!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (error)
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Unable to send email!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
    }
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - Redefined Methods

- (void)setFilename:(NSString *)filename
{
    _filename = filename;
    NSString *tmpStr = [PHFECommon getFileNameFromFullPath:filename];
    tmpStr = [PHFECommon stringWithoutSubStringAtTheFront:tmpStr andSubString:@"b_" andCaseSensitive:NO];
    tmpStr = [PHFECommon getFileNameWithoutExtension:tmpStr];
    self.title = tmpStr;
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.activitityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activitityIndicator stopAnimating];
}


@end
