//
//  BasicContentViewController.h
//  Sidekick
//
//  Created by Ricol Wang on 12/04/13.
//
//

#import <UIKit/UIKit.h>
#import <PHCommon/utils.h>

#define ACTIVITY_INDICATOR_BOTTOM_MARGIN 80

@interface BasicContentViewController : UIViewController <UIWebViewDelegate>

@property (strong) UIWebView * contentWebView;
@property (strong) UIActivityIndicatorView *activitityIndicator;

@property (strong, nonatomic) NSString *filename;

@property (strong) UIButton *btnEmail;
@property (strong) UIButton *btnPrint;
@property (strong) UIButton *btnInfor;

- (void)GoBack:(id)Sender;
- (void)GoInformation:(id)Sender;
- (void)GoHome:(id)Sender;
- (void)GoPrint:(id)Sender;
- (void)GoPrintWithFileName:(NSString *)filename andSender:(id)Sender;
- (void)GoEmail:(id)Sender;
- (void)GoEmailWithFileName:(NSString *)filename;

@end
