//
//  WebContentViewController.m
//  Sidekick
//
//  Created by Ricol Wang on 12/04/13.
//
//

#import "WebContentViewController.h"
#import "PHFECommon.h"

#define LIBRARY_DIRECTORY [NSHomeDirectory() stringByAppendingPathComponent:@"Library"]

@interface WebContentViewController ()

@end

@implementation WebContentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
    self.contentWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.contentWebView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.contentWebView.backgroundColor = [UIColor clearColor];
    self.contentWebView.delegate = self;
    [self.view addSubview:self.contentWebView];
    
    self.activitityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activitityIndicator.color = [UIColor blueColor];
    
    NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] initWithArray:self.navigationItem.rightBarButtonItems];
    UIBarButtonItem *tmpBar = [[UIBarButtonItem alloc] initWithCustomView:self.activitityIndicator];
    [tmpMutableArray addObject:tmpBar];
    self.navigationItem.rightBarButtonItems = tmpMutableArray;
    //    self.activitityIndicator.frame = CGRectMake(self.view.bounds.size.width / 2 - 25, self.view.bounds.size.height - ACTIVITY_INDICATOR_BOTTOM_MARGIN, 50, 50);
    //    self.activitityIndicator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    self.activitityIndicator.hidesWhenStopped = YES;
    //    [self.view addSubview:self.activitityIndicator];
}

- (void) showWebResourceAt:(NSString *)resourcePath
{
    self.filename = resourcePath;
    NSString *tmpContent = [NSString stringWithContentsOfFile:resourcePath encoding:NSUTF8StringEncoding error:nil];
    NSURL *tmpURL = [NSURL URLWithString:tmpContent];
    NSURLRequest *tmpURLRequest = [NSURLRequest requestWithURL:tmpURL];
    [self.contentWebView loadRequest:tmpURLRequest];
}

- (void) closeView
{
    [self.contentWebView loadHTMLString:@"HELLO" baseURL:nil];
    [self.contentWebView removeFromSuperview];
    self.contentWebView = nil;
}

#pragma mark - Override Methods

- (void)GoEmail:(id)Sender
{    
    NSError *error = nil;
    NSString *tmpStr_Content = [self.contentWebView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
    NSString *tmpStr_Title = [self.contentWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
    
    if (error)
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Unable to email the file!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
    }else
    {
        NSString *tmpStr_FileName = [NSString stringWithFormat:@"%@/%@.html", LIBRARY_DIRECTORY, tmpStr_Title];
        error = nil;
        [tmpStr_Content writeToFile:tmpStr_FileName atomically:YES encoding:NSUTF8StringEncoding error:&error];
        if (error)
        {
            UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Unable to email the file!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [tmpAlertView show];
        }else
        {
            [super GoEmailWithFileName:tmpStr_FileName]; 
        }
    }
}


- (void) loadWithLocalContent:(NSString *)contentPath{
    NSURL *tmpURL = [NSURL URLWithString:[contentPath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *tmpURLRequest = [NSURLRequest requestWithURL:tmpURL];
    [self.contentWebView loadRequest:tmpURLRequest];
}

- (void)GoPrint:(id)Sender
{
    NSError *error = nil;
    NSString *tmpStr_Content = [self.contentWebView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
    NSString *tmpStr_Title = [self.contentWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
    if (error)
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Unable to email the file!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
    }else
    {
        NSString *tmpStr_FileName = [NSString stringWithFormat:@"%@/%@.html", LIBRARY_DIRECTORY, tmpStr_Title];
        error = nil;
        [tmpStr_Content writeToFile:tmpStr_FileName atomically:YES encoding:NSUTF8StringEncoding error:&error];
        if (error)
        {
            UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Unable to email the file!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [tmpAlertView show];
        }else
        {
            [super GoPrintWithFileName:tmpStr_FileName andSender:Sender];
        }
    }
}

@end
