//
//  MediaPlayerViewController.h
//  GMGridViewExample
//
//  Created by Philip Cookson on 10/07/12.
//  Copyright (c) 2012 GMoledina.ca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "BasicContentViewController.h"

@interface MediaPlayerViewController : BasicContentViewController

@property BOOL bVideoIsFullScreen;

- (id)initWithFrame:(CGRect)frame;
- (void) playMediaAt:(NSString *)mediaPath;

@end
