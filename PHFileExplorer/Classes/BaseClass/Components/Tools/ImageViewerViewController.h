//
//  ImageViewerViewController.h
//  GMGridViewExample
//
//  Created by Philip Cookson on 11/07/12.
//  Copyright (c) 2012 GMoledina.ca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZoomingScrollView.h"
#import "BasicContentViewController.h"

@interface ImageViewerViewController : BasicContentViewController

- (id)initWithFrame:(CGRect)frame andDirectory:(NSString *)directory;
- (void) showImageAt:(NSString *)imagePath;
- (void) closeView;

@end
