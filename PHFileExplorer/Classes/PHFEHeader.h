//
//  Header.h
//  Sidekick
//
//  Created by Ricol Wang on 16/04/13.
//
//

#ifndef PHFileExplorer_Header
#define PHFileExplorer_Header

//#define _DEBUG

#define PHFE_IMAGE_GALLERY_DIRECTORY [NSHomeDirectory() stringByAppendingPathComponent:@"Library/ImageGallery"]
#define PHFE_IMAGE_GALLERY_THUMBS_DIRECTORY [NSHomeDirectory() stringByAppendingPathComponent:@"Library/ImageGalleryThumbs"]

#define PHFE_TIME_INFORDIALOG 2
#define PHFE_TIME_ANIMATION 0.25
#define PHFE_TIME_DELAY 0.2

#define PHFE_TAG_ALERTVIEW_DELETE 100
#define PHFE_TAG_ALERTVIEW_ZIP 101

#define String(s) NSLocalizedStringFromTable(s, @"PHFE", nil)

#endif
