//
//  PhotoBrowserViewController.m
//  Sidekick
//
//  Created by Ricol Wang on 1/07/13.
//
//

#import "PhotoBrowserViewController.h"
#import <GVPhotoBrowser.h>
#import <MessageUI/MessageUI.h>
#import <ImageViewWithGesture.h>
#import <PHCommon/utils.h>
#import "PHFECommon.h"
#import "PHFEHeader.h"
#import "FullScreenImageViewerViewController.h"

@interface PhotoBrowserViewController () <GVPhotoBrowserDataSource, GVPhotoBrowserDelegate, MFMailComposeViewControllerDelegate, ImageViewWithGestureDelegate, FullScreenImageViewerVCDelegate>

{
    BOOL bFirstTime;
}

@property (weak, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentIndex;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (strong, nonatomic) IBOutlet UIView *viewToolBar;

@property NSString *ImagePath;
@property NSString *ImageName;
@property NSArray *arrayImages;
@property GVPhotoBrowser *photoBrowser;
@property int currentIndex;

- (IBAction)btnEmailOnTapped:(id)sender;

- (void)LoadData;

@end

@implementation PhotoBrowserViewController

- (id)init
{
    self = [super initWithNibName:@"PhotoBrowserViewController" bundle:nil];
    if (self)
    {
        self.ImagePath = PHFE_IMAGE_GALLERY_DIRECTORY;
        self.ImageName = nil;
        self.arrayImages = [NSArray new];
        bFirstTime = YES;
    }
    return self;
}

- (id)initWithImagePath:(NSString *)imagePath withName:(NSString *)imageName
{
    self = [super initWithNibName:@"PhotoBrowserViewController" bundle:nil];
    if (self)
    {
        self.ImagePath = [imagePath copy];
        self.ImageName = [imageName copy];
        self.arrayImages = [NSArray new];
        bFirstTime = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *tmpBarBtnItemRight = [[UIBarButtonItem alloc] initWithCustomView:self.viewToolBar];
    self.navigationItem.rightBarButtonItem = tmpBarBtnItemRight;
    
    if ([utils isIOS7])
    {
        if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        {
            self.edgesForExtendedLayout = UIRectEdgeNone;
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.title = @"IMAGE";
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (bFirstTime)
    {
        [self LoadData];
        
        self.photoBrowser = [[GVPhotoBrowser alloc] initWithFrame:self.view.bounds];
        self.photoBrowser.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.photoBrowser.dataSource = self;
        self.photoBrowser.delegate = self;
        [self.view addSubview:self.photoBrowser];
        
        if (self.ImageName)
            self.photoBrowser.currentIndex = [self.arrayImages indexOfObject:self.ImageName];
        else
            self.photoBrowser.currentIndex = 0;
        
        self.currentIndex = self.photoBrowser.currentIndex;
        
        bFirstTime = NO;
        
        NSString *tmpFileName;
        if (self.arrayImages.count <= 0)
        {
            self.title = @"Images";
        }else
        {
            tmpFileName = self.arrayImages[self.photoBrowser.currentIndex];
            self.title = [PHFECommon stringWithoutSubStringAtTheFront:tmpFileName andSubString:@"b_" andCaseSensitive:NO];
        }
    }
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [UIView animateWithDuration:PHFE_TIME_ANIMATION animations:^(){
        self.photoBrowser.alpha = 0;
    } completion:^(BOOL finished){
        [self.photoBrowser removeFromSuperview];
    }];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    self.photoBrowser = [[GVPhotoBrowser alloc] initWithFrame:self.view.bounds];
    self.photoBrowser.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.photoBrowser.dataSource = self;
    self.photoBrowser.delegate = self;
    self.photoBrowser.alpha = 0;
    [self.view addSubview:self.photoBrowser];
    
    NSString *tmpFileName;
    if (self.arrayImages.count <= 0)
    {
        self.title = @"Images";
    }else
    {
        tmpFileName = self.arrayImages[self.currentIndex];
        self.title = [PHFECommon stringWithoutSubStringAtTheFront:tmpFileName andSubString:@"b_" andCaseSensitive:NO];
    }
    
    self.photoBrowser.currentIndex = self.currentIndex;
    
    [UIView animateWithDuration:PHFE_TIME_ANIMATION animations:^(){
        self.photoBrowser.alpha = 1;
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setLblCurrentIndex:nil];
    [self setLblTotal:nil];
    [self setBtnEmail:nil];
    [self setPhotoBrowser:nil];
    [super viewDidUnload];
}

#pragma mark - GVPhotoBrowserDataSource

- (NSUInteger)numberOfPhotosInPhotoBrowser:(GVPhotoBrowser *)photoBrowser
{
    return self.arrayImages.count;
}

- (UIImageView *)photoBrowser:(GVPhotoBrowser *)photoBrowser customizeImageView:(UIImageView *)imageView forIndex:(NSUInteger)index
{
    NSString *tmpFileName = self.arrayImages[index];
    imageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", self.ImagePath, tmpFileName]];
    return imageView;
}

- (UIImageView *)baseImageViewForPhotoBrowser:(GVPhotoBrowser *)photoBrowser withFrame:(CGRect)frame
{
    ImageViewWithGesture *tmpImageWithGesture = [[ImageViewWithGesture alloc] initWithFrame:frame];
    tmpImageWithGesture.userInteractionEnabled = YES;
    tmpImageWithGesture.clipsToBounds = YES;
    tmpImageWithGesture.contentMode = UIViewContentModeScaleAspectFit;
    tmpImageWithGesture.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tmpImageWithGesture.backgroundColor = [UIColor clearColor];
    tmpImageWithGesture.delegate = self;
    
    [tmpImageWithGesture RegisterDoubleTapGesture:YES];
    
    return tmpImageWithGesture;
}

#pragma mark - GVPhotoBrowserDelegate

- (void)photoBrowser:(GVPhotoBrowser *)photoBrowser didSwitchToIndex:(NSUInteger)index
{
    self.lblCurrentIndex.text = [NSString stringWithFormat:@"%d", index + 1];
    self.lblTotal.text = [NSString stringWithFormat:@"%d", self.arrayImages.count];
    NSString *tmpFileName = self.arrayImages[index];
    self.title = [PHFECommon stringWithoutSubStringAtTheFront:tmpFileName andSubString:@"b_" andCaseSensitive:NO];
    self.currentIndex = index;
}

#pragma mark - Private Methods

- (void)LoadData
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    NSArray *tmpArray = [fm contentsOfDirectoryAtPath:self.ImagePath error:&error];
    if (error)
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Unable to access images!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
    }else if (tmpArray.count <= 0)
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"MESSAGE" message:@"No Image Available!\nImages can be added to the image gallery from the settings." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
    }else
    {
        //filter images
        NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] init];
        for (int i = 0; i < tmpArray.count; i++)
        {
            NSString *tmpStrFileName = tmpArray[i];
            NSString *tmpStrFileExtension = [PHFECommon getFileExtension:tmpStrFileName];
            if (!tmpStrFileExtension) continue;
            tmpStrFileExtension = [tmpStrFileExtension lowercaseString];
            if ([tmpStrFileExtension isEqualToString:@"jpg"] || [tmpStrFileExtension isEqualToString:@"png"])
                [tmpMutableArray addObject:tmpStrFileName];
        }
        self.arrayImages = [[NSArray alloc] initWithArray:tmpMutableArray];
    }
}

#pragma mark - IBActions

- (IBAction)btnEmailOnTapped:(id)sender
{
    if(![MFMailComposeViewController canSendMail])
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"WARNING" message:@"You must setup an email account in order to use this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
    }
    
    if (self.arrayImages.count > 0)
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        NSString *tmpFileName = [NSString stringWithFormat:@"%@/%@", self.ImagePath, self.arrayImages[self.photoBrowser.currentIndex]];
        NSFileManager *fm = [NSFileManager defaultManager];
        
        if ([fm fileExistsAtPath:tmpFileName])
        {
            NSData *tmpData = [[NSData alloc] initWithContentsOfFile:tmpFileName];
            
            NSString *tmpSubject = [NSString stringWithFormat:@"Sent from Images Gallery: %@", self.arrayImages[self.photoBrowser.currentIndex]];
            [mail addAttachmentData:tmpData mimeType:@"image/png" fileName:self.arrayImages[self.photoBrowser.currentIndex]];
            mail.mailComposeDelegate = self; // &lt;- very important step if you want feedbacks on what the user did with your email sheet
            [mail setSubject:tmpSubject];
            [mail setMessageBody:tmpSubject isHTML:YES]; // depends. Mostly YES, unless you want to send it as plain text (boring)
            mail.navigationBar.barStyle = UIBarStyleDefault; // choose your style, unfortunately, Translucent colors behave quirky.
            [self presentViewController:mail animated:YES completion:nil];
        }else
        {
            UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"WARNING" message:@"Image doesn't exist!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [tmpAlertView show];
        }
    }else
    {
        UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"WARNING" message:@"No Images to send!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [tmpAlertView show];
    }
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
			break;
		case MFMailComposeResultSaved:
			break;
		case MFMailComposeResultSent:
			break;
		case MFMailComposeResultFailed:
        {
			UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Sending Failed!\nPlease check your mailbox setup!"
                                                                  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[tmpAlertView show];
            break;
		}
			
		default:
		{
			UIAlertView *tmpAlertView = [[UIAlertView alloc] initWithTitle:@"ERROR" message:@"Sending Failed - Unknown Error - Please check your mailbox setup"
														   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
			[tmpAlertView show];
            break;
		}
	}
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - ImageViewWithGestureDelegate

- (void)ImageViewWithGestureDoubleTapped:(ImageViewWithGesture *)imageView
{
//    UIImage *tmpImage = imageView.image;
//    
//    self.navigationController.navigationBarHidden = !self.navigationController.navigationBarHidden;
//    
//    FullScreenImageViewerViewController *tmpVC = [[FullScreenImageViewerViewController alloc] initWithImage:tmpImage];
//    tmpVC.modalPresentationStyle = UIModalPresentationFullScreen;
//    tmpVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    tmpVC.delegate = self;
//    [self presentViewController:tmpVC animated:YES completion:nil];
}

#pragma mark - FullScreenImageViewerVCDelegate

- (void)FullScreenImageViewerVCExit:(FullScreenImageViewerViewController *)vc
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
