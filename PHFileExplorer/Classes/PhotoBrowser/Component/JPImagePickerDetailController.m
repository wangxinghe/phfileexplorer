//
//  JPImagePickerDetailController.m
//  JPImagePickerController
//
//  Created by Jeena on 11.11.09.
//  Copyright 2009 Jeena Paradies.
//  Licence: MIT-Licence
//

#import "JPImagePickerDetailController.h"
#import "PHFEHeader.h"



@implementation JPImagePickerDetailController

@synthesize previewImageView, overviewController, imageNumber;

- (id)initWithOverviewController:(JPImagePickerOverviewController *)newOverviewController {
	if (self = [super initWithNibName:@"JPImagePickerDetailController" bundle:nil]) {
        // Custom initialization
		overviewController = newOverviewController;
		imageNumber = -1;
    }
    return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}


- (void)viewWillAppear:(BOOL)animated {
	originalStatusBarStyle = [UIApplication sharedApplication].statusBarStyle;
	[super viewWillAppear:animated];
	[self.navigationController setNavigationBarHidden:YES animated:YES];
	[self prepareForImageNumber:imageNumber];
}

/*
- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
}
*/
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
	[self.navigationController setNavigationBarHidden:NO animated:YES];
}

/*
 - (void)viewDidDisappear:(BOOL)animated {
 [super viewDidDisappear:animated];
 }
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/


- (void)prepareForImageNumber:(NSInteger)newImageNumber {
	imageNumber = newImageNumber;
	previewImageView.image = [[overviewController.imagePickerController.dataSource
							   imagePicker:overviewController.imagePickerController
							   imageForImageNumber:imageNumber]
							  scaleToSize:CGSizeMake(kJPImagePickerControllerPreviewImageSizeWidth, kJPImagePickerControllerPreviewImageSizeHeight)
							  onlyIfNeeded:YES];
}

- (IBAction)cancelPreview:(id)sender {
	[[UIApplication sharedApplication] setStatusBarStyle:originalStatusBarStyle animated:YES];
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)finishedPicking:(id)sender {
	NSFileManager *fileMgr = [NSFileManager defaultManager];
	//use imageNumber, remove this image from folio folder
	NSError *error;
	//remove thumb
	NSMutableArray *folioImagesThumbs = (NSMutableArray *)[[NSFileManager defaultManager] contentsOfDirectoryAtPath:PHFE_IMAGE_GALLERY_THUMBS_DIRECTORY error:&error];
	[fileMgr removeItemAtPath:[NSString stringWithFormat:@"%@/%@", PHFE_IMAGE_GALLERY_THUMBS_DIRECTORY, [folioImagesThumbs objectAtIndex:imageNumber]] error:&error];

	
	NSMutableArray *folioImages = (NSMutableArray *)[[NSFileManager defaultManager] contentsOfDirectoryAtPath:PHFE_IMAGE_GALLERY_DIRECTORY error:&error];
	[fileMgr removeItemAtPath:[NSString stringWithFormat:@"%@/%@", PHFE_IMAGE_GALLERY_DIRECTORY, [folioImages objectAtIndex:imageNumber]] error:&error];

	
	[overviewController resetView];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


@end
