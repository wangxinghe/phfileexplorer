//
//  FullScreenImageViewerVCDelegate.h
//  Sidekick
//
//  Created by Ricol Wang on 16/09/13.
//
//

#import <Foundation/Foundation.h>

@class FullScreenImageViewerViewController;

@protocol FullScreenImageViewerVCDelegate <NSObject>

- (void)FullScreenImageViewerVCExit:(FullScreenImageViewerViewController *)vc;

@end
