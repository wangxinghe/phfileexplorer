//
//  FullScreenImageViewerViewController.m
//  Sidekick
//
//  Created by Ricol Wang on 16/09/13.
//
//

#import "FullScreenImageViewerViewController.h"

@interface FullScreenImageViewerViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;

@property (strong) UIImage *image;

- (IBAction)btnCloseOnTapped:(id)sender;

@end

@implementation FullScreenImageViewerViewController

- (id)initWithImage:(UIImage *)image
{
    self = [super initWithNibName:@"FullScreenImageViewerViewController" bundle:nil];
    if (self) {
        // Custom initialization
        self.image = image;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.imageView.image = self.image;
    self.scrollView.minimumZoomScale = 0.5;
    self.scrollView.maximumZoomScale = 2;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setImageView:nil];
    [self setBtnClose:nil];
    [super viewDidUnload];
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

#pragma mark - IBAction Methods

- (IBAction)btnCloseOnTapped:(id)sender
{
    [self.delegate FullScreenImageViewerVCExit:self];
}

@end
