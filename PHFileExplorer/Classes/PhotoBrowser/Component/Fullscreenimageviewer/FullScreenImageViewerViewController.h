//
//  FullScreenImageViewerViewController.h
//  Sidekick
//
//  Created by Ricol Wang on 16/09/13.
//
//

#import <UIKit/UIKit.h>
#import "FullScreenImageViewerVCDelegate.h"

@interface FullScreenImageViewerViewController : UIViewController

- (id)initWithImage:(UIImage *)image;

@property (weak) id <FullScreenImageViewerVCDelegate> delegate;

@end
