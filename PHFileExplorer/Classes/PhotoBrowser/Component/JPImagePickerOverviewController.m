//
//  JPImagePickerOverviewController.m
//  JPImagePickerController
//
//  Created by Jeena on 11.11.09.
//  Copyright 2009 Jeena Paradies.
//  Licence: MIT-Licence
//

#import "JPImagePickerOverviewController.h"
#import "PHFEHeader.h"

@implementation JPImagePickerOverviewController

@synthesize imagePickerController, detailController, scrollView, popoverController;

#define PADDING_TOP 44
#define PADDING 18
#define THUMBNAIL_COLS 6


- (id)initWithImagePickerController:(JPImagePickerController *)newImagePickerController {
    if (self = [super initWithNibName:@"JPImagePickerOverviewController" bundle:nil]) {
        // Custom initialization
		imagePickerController = newImagePickerController;
    }
    return self;
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	popOverVisible = NO;
	[self setImagePickerTitle:imagePickerController.imagePickerTitle];
	
	UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
																				  target:self
																				  action:@selector(cancelPicking:)];
	
	addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
																				  target:self
																				  action:@selector(addImage:)];
	self.navigationItem.leftBarButtonItem = cancelButton;
	self.navigationItem.rightBarButtonItem = addButton;
	self.navigationItem.title = @"Folio Settings";

	
	
	UIButton *button;
	UIImage *thumbnail;
	int images_count = [imagePickerController.dataSource numberOfImagesInImagePicker:imagePickerController];
	
	for (int i=0; i<images_count; i++) {
		thumbnail = [[imagePickerController.dataSource imagePicker:imagePickerController thumbnailForImageNumber:(NSInteger)i]
					 scaleAndCropToSize:CGSizeMake(kJPImagePickerControllerThumbnailSizeWidth, kJPImagePickerControllerThumbnailSizeHeight)
					 onlyIfNeeded:NO];
		
		button = [UIButton buttonWithType:UIButtonTypeCustom];
		[button setImage:thumbnail forState:UIControlStateNormal];
		button.showsTouchWhenHighlighted = YES;
		button.userInteractionEnabled = YES;
		[button addTarget:self action:@selector(buttonTouched:) forControlEvents:UIControlEventTouchUpInside];
		button.tag = i;
		button.frame = CGRectMake(kJPImagePickerControllerThumbnailSizeWidth * (i % THUMBNAIL_COLS) + PADDING * (i % THUMBNAIL_COLS) + PADDING,
								  kJPImagePickerControllerThumbnailSizeHeight * (i / THUMBNAIL_COLS) + PADDING * (i / THUMBNAIL_COLS) + PADDING + PADDING_TOP,
								  kJPImagePickerControllerThumbnailSizeWidth,
								  kJPImagePickerControllerThumbnailSizeHeight);
		
		[scrollView addSubview:button];
	}
	
	int rows = images_count / THUMBNAIL_COLS;
	if (((float)images_count / THUMBNAIL_COLS) - rows != 0) {
		rows++;
	}
	int height = kJPImagePickerControllerThumbnailSizeHeight * rows + PADDING * rows + PADDING + PADDING_TOP;
	
	scrollView.contentSize = CGSizeMake(self.view.frame.size.width, height);
	scrollView.clipsToBounds = YES;
	scrollView.backgroundColor = [UIColor blackColor];
	
}

- (void)setImagePickerTitle:(NSString *)newTitle {
	self.navigationItem.title = newTitle;
}

- (NSString *)imagePickerTitle {
	return self.navigationItem.title;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    return (orientation != UIDeviceOrientationPortrait) &&
	(orientation != UIDeviceOrientationPortraitUpsideDown);
}

- (IBAction)cancelPicking:(id)sender {
	[self.popoverController dismissPopoverAnimated:NO];
	[[UIApplication sharedApplication] setStatusBarStyle:imagePickerController.originalStatusBarStyle animated:YES];
	[imagePickerController.delegate imagePickerDidCancel:imagePickerController];
}


- (void)buttonTouched:(UIButton *)sender {
	if (detailController == nil) {
		detailController = [[JPImagePickerDetailController alloc] initWithOverviewController:self];
	}
	[detailController prepareForImageNumber:(NSInteger)sender.tag];
	[imagePickerController.modalNavigationController pushViewController:detailController animated:YES];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}

- (IBAction)addImage:(id)sender{
	if(popOverVisible == NO){
		popOverVisible = YES;
		UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
		imagePicker.delegate = self;
		imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
		
		
		//resize the popover view shown
		//in the current view to the view's size
		imagePicker.contentSizeForViewInPopover = CGSizeMake(320, 500);
		
		//create a popover controller
		UIPopoverController *popOver = [[UIPopoverController alloc]initWithContentViewController:imagePicker];
		popOver.delegate = self;
		self.popoverController = popOver;
		//present the popover view non-modal with a
		//refrence to the button pressed within the current view
		[self.popoverController presentPopoverFromBarButtonItem:addButton permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
		
	
	}
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{ 
	NSError *error;
	NSMutableArray *images = (NSMutableArray *)[[NSFileManager defaultManager] contentsOfDirectoryAtPath:PHFE_IMAGE_GALLERY_DIRECTORY error:&error];

	int i = 0;
	for(int c=0;c<[images count];c++){
		NSArray *tempArray = [[images objectAtIndex:c] componentsSeparatedByString:@"."];		
		if([[tempArray objectAtIndex:0] intValue] > i){
			i = [[tempArray objectAtIndex:0] intValue];
		}
	}
	i++;
	
	NSString *nextNumIncStrFormatted;
	NSString *nextNumIncrStr = [[NSString alloc] initWithFormat:@"%d", i];	
	//format number appopriately to add to primary key
	if([nextNumIncrStr length] == 1){
		nextNumIncStrFormatted = [@"0000" stringByAppendingString:nextNumIncrStr];
	}
	else if([nextNumIncrStr length] == 2){
		nextNumIncStrFormatted = [@"000" stringByAppendingString:nextNumIncrStr];
		
	}
	else if([nextNumIncrStr length] == 3){
		nextNumIncStrFormatted = [@"00" stringByAppendingString:nextNumIncrStr];
	}
	else if([nextNumIncrStr length] == 4){
		nextNumIncStrFormatted = [@"0" stringByAppendingString:nextNumIncrStr];
	}
	else{
		nextNumIncStrFormatted = nextNumIncrStr;
	}
	
	UIImage* image = [info valueForKey:@"UIImagePickerControllerOriginalImage"];


	
	NSData *pngImage = UIImagePNGRepresentation(image);
	NSString *filePath = [[NSString alloc]initWithFormat:@"%@/%@.png", PHFE_IMAGE_GALLERY_DIRECTORY, nextNumIncStrFormatted];
	[pngImage writeToFile:filePath atomically:YES];

	
	UIGraphicsBeginImageContext(CGSizeMake(150.0,150.0));
	[image drawInRect:CGRectMake(0.0, 0.0, 150.0, 150.0)];
	// make a "copy" of the image from the current context
	UIImage* imageThumb = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();	
	NSData  *thumbData = UIImagePNGRepresentation(imageThumb);
	NSString *filePathThumb = [[NSString alloc]initWithFormat:@"%@/%@_thumb.png", PHFE_IMAGE_GALLERY_THUMBS_DIRECTORY, nextNumIncStrFormatted];
	[thumbData writeToFile:filePathThumb atomically:YES];

	[self.popoverController dismissPopoverAnimated:NO];	

	[imagePickerController.delegate imagePickerReset:imagePickerController];
}
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
	popOverVisible = NO;
}
- (void)resetView{
	[imagePickerController.delegate imagePickerReset:imagePickerController];	
}

@end
