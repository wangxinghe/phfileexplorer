//
//  JPImagePickerOverviewController.h
//  JPImagePickerController
//
//  Created by Jeena on 11.11.09.
//  Copyright 2009 Jeena Paradies.
//  Licence: MIT-Licence
//

#import <UIKit/UIKit.h>
#import "JPImagePickerController.h"
#import "JPImagePickerDetailController.h"
#import "UIImageResizing.h"

@class JPImagePickerController, JPImagePickerDetailController;
@protocol JPImagePickerControllerDelegate, JPImagePickerControllerDataSource;

@interface JPImagePickerOverviewController : UIViewController  <UIPopoverControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
	JPImagePickerController *imagePickerController;
	JPImagePickerDetailController *detailController;
	IBOutlet UIScrollView *scrollView;
	UIBarButtonItem *addButton;
	UIPopoverController *popoverController;
	BOOL popOverVisible;
}

@property (nonatomic, retain, readonly) JPImagePickerController *imagePickerController;
@property (nonatomic, retain) JPImagePickerDetailController *detailController;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain)UIPopoverController *popoverController;

- (id)initWithImagePickerController:(JPImagePickerController *)newImagePickerController;
- (void)setImagePickerTitle:(NSString *)newTitle;
- (NSString *)imagePickerTitle;
- (IBAction)cancelPicking:(id)sender;
- (IBAction)addImage:(id)sender;
- (void)buttonTouched:(UIButton *)sender;
- (void)resetView;

@end
