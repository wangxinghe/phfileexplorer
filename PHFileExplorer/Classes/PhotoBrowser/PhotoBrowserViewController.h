//
//  PhotoBrowserViewController.h
//  Sidekick
//
//  Created by Ricol Wang on 1/07/13.
//
//

#import <UIKit/UIKit.h>

@interface PhotoBrowserViewController : UIViewController

- (id)initWithImagePath:(NSString *)imagePath withName:(NSString *)imageName;

@end
