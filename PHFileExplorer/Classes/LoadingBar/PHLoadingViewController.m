//
//  PHLoadingViewController.m
//  NESTAB
//
//  Created by Ricol Wang on 19/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import "PHLoadingViewController.h"
#import "PHFEHeader.h"

@interface PHLoadingViewController ()

@property (strong) NSTimer *timer;
@property int count;

@end

@implementation PHLoadingViewController

- (id)init
{
    self = [super initWithNibName:@"PHLoadingViewController" bundle:nil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(UpdateMessage) userInfo:nil repeats:YES];
    self.count = 0;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.timer invalidate];
    self.timer = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setLblMessage:nil];
    [super viewDidUnload];
}

- (void)UpdateMessage
{
    if (self.count == 0)
        dispatch_async(dispatch_get_main_queue(), ^(){
        self.lblMessage.text = String(@"Loading");
        });
    else if (self.count == 1)
        dispatch_async(dispatch_get_main_queue(), ^(){
            self.lblMessage.text = [NSString stringWithFormat:@"%@.", String(@"Loading")];
        });
    else if (self.count == 2)
        dispatch_async(dispatch_get_main_queue(), ^(){
            self.lblMessage.text = [NSString stringWithFormat:@"%@..", String(@"Loading")];
        });
    else if (self.count == 3)
        dispatch_async(dispatch_get_main_queue(), ^(){
            self.lblMessage.text = [NSString stringWithFormat:@"%@...", String(@"Loading")];
        });
    self.count++;
    if (self.count > 3) self.count = 0;
}

@end
