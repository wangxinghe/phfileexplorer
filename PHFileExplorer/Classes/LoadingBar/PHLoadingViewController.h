//
//  PHLoadingViewController.h
//  NESTAB
//
//  Created by Ricol Wang on 19/03/13.
//  Copyright (c) 2013 Philology Pty. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PHLoadingViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;

@end
