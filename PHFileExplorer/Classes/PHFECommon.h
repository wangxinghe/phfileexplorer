//
//  PHFECommon.h
//
//  Created by ricol wang on 7/11/12.
//
//

#import <Foundation/Foundation.h>

typedef enum FILETYPE
{
    ISDIRECTORY, ISFILE, ISPDFFILE, ISZIPFILE, UNKNOW, ISOTHERTYPEFILE
}FILETYPE;

@interface PHFECommon : NSObject

+ (NSString *) getTypeFromExtension:(NSString *)extension;
+ (NSString *)getFileExtension:(NSString *)file;
+ (NSString *)getFileNameFromFullPath:(NSString *)filepath;
+ (NSString *)stringWithoutSubStringAtTheFront:(NSString *)originalString andSubString:(NSString *)subString andCaseSensitive:(BOOL)sensitive;
+ (NSString *)getFileNameWithoutExtension:(NSString *)filename;
+ (NSArray *)getAllFilesUnderDirectory:(NSString *)directory andSubDirectory:(BOOL)subDirectory;

@end
