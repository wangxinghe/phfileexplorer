//
//  PHNavigateDelegate.h
//  PHFileExplorer
//
//  Created by Ricol Wang on 7/08/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PHNavigateDelegate <NSObject>

- (void)ShowPreviousItem;
- (void)ShowNextItem;

@end
