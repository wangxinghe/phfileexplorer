//
//  PHFECommon.m
//
//  Created by ricol wang on 7/11/12.
//
//

#import "PHFECommon.h"

@implementation PHFECommon

#pragma mark - static functions

+ (NSString *) getTypeFromExtension:(NSString *)extension
{
    if(extension)
    {
        extension = [extension uppercaseString];
        
        if([extension isEqualToString:@"BS"] || [extension isEqualToString:@"BLO"] || [extension isEqualToString:@"BLOGSPOT"])
        {
            return @"Blogspot";
        }
        else if([extension isEqualToString:@"FB"] || [extension isEqualToString:@"FAC"] || [extension isEqualToString:@"FACEBOOK"])
        {
            return @"Facebook";
        }
        else if([extension isEqualToString:@"RS"] || [extension isEqualToString:@"RSS"])
        {
            return @"RSS";
        }
        else if([extension isEqualToString:@"YT"] || [extension isEqualToString:@"YOU"] || [extension isEqualToString:@"YOUTUBE"])
        {
            return @"Youtube";
        }
        else if([extension isEqualToString:@"TW"] || [extension isEqualToString:@"TWI"] || [extension isEqualToString:@"TWIT"] || [extension isEqualToString:@"TWITTER"])
        {
            return @"Twitter";
        }
        else if([extension isEqualToString:@"ZIP"] || [extension isEqualToString:@"RAR"] || [extension isEqualToString:@"TAR"] || [extension isEqualToString:@"GZ"])
        {
            return @"Compressed";
        }
        else if([extension isEqualToString:@"PDF"] || [extension isEqualToString:@"PRN"])
        {
            return @"PDF";
        }
        else if([extension isEqualToString:@"PAGES"])
        {
            return @"Pages";
        }
        else if([extension isEqualToString:@"KEY"] || [extension isEqualToString:@"KEYNOTE"])
        {
            return @"Keynote";
        }
        else if([extension isEqualToString:@"URL"] || [extension isEqualToString:@"HTM"] || [extension isEqualToString:@"HTML"] || [extension isEqualToString:@"MHT"] || [extension isEqualToString:@"MHTML"] || [extension isEqualToString:@"ASP"] || [extension isEqualToString:@"ASPX"] || [extension isEqualToString:@"PHP"] || [extension isEqualToString:@"XHTML"] || [extension isEqualToString:@"XPS"] || [extension isEqualToString:@"WWW"])
        {
            return @"Web";
        }
        else if([extension isEqualToString:@"DOC"] || [extension isEqualToString:@"DOCX"] || [extension isEqualToString:@"DOT"] || [extension isEqualToString:@"DOTM"] || [extension isEqualToString:@"DOTX"] || [extension isEqualToString:@"MCW"] || [extension isEqualToString:@"WPS"] || [extension isEqualToString:@"WPT"] || [extension isEqualToString:@"WRI"])
        {
            return @"Word";
        }
        else if ([extension isEqualToString:@"CSV"] || [extension isEqualToString:@"DBF"] || [extension isEqualToString:@"DIF"] || [extension isEqualToString:@"ODS"] || [extension isEqualToString:@"SLK"] || [extension isEqualToString:@"XLA"] || [extension isEqualToString:@"XLAM"] || [extension isEqualToString:@"XLS"] || [extension isEqualToString:@"XLSB"] || [extension isEqualToString:@"XLSM"] || [extension isEqualToString:@"XLSX"] || [extension isEqualToString:@"XLT"] || [extension isEqualToString:@"XLTM"] || [extension isEqualToString:@"XLTX"] || [extension isEqualToString:@"XLW"] || [extension isEqualToString:@"NUMBERS"])
        {
            return @"Spreadsheet";
        }
        else if([extension isEqualToString:@"EMF"] || [extension isEqualToString:@"ODP"] || [extension isEqualToString:@"POT"] || [extension isEqualToString:@"POTM"] || [extension isEqualToString:@"POTX"] || [extension isEqualToString:@"PPA"] || [extension isEqualToString:@"PPAM"] || [extension isEqualToString:@"PPS"] || [extension isEqualToString:@"PPSM"] || [extension isEqualToString:@"PPSX"] || [extension isEqualToString:@"PPT"] || [extension isEqualToString:@"PPTM"] || [extension isEqualToString:@"PPTX"] || [extension isEqualToString:@"THMX"] || [extension isEqualToString:@"WMF"])
        {
            return @"Powerpoint";
        }
        else if([extension isEqualToString:@"AAF"] || [extension isEqualToString:@"3GP"] || [extension isEqualToString:@"ASF"] || [extension isEqualToString:@"AVCHD"] || [extension isEqualToString:@"AVI"] || [extension isEqualToString:@"FLV"] || [extension isEqualToString:@"M1V"] || [extension isEqualToString:@"M2V"] || [extension isEqualToString:@"M4V"] || [extension isEqualToString:@"MPEG-1"] || [extension isEqualToString:@"MPEG-2"] || [extension isEqualToString:@"MPEG-4"] || [extension isEqualToString:@"MPV"] || [extension isEqualToString:@"MP4"] || [extension isEqualToString:@"SOL"] || [extension isEqualToString:@"MNG"] || [extension isEqualToString:@"MPEG"] || [extension isEqualToString:@"MPG"] || [extension isEqualToString:@"MPE"] || [extension isEqualToString:@"NSV"] || [extension isEqualToString:@"OGG"] || [extension isEqualToString:@"RM"] || [extension isEqualToString:@"SVI"] || [extension isEqualToString:@"SMI"] || [extension isEqualToString:@"SWF"] || [extension isEqualToString:@"WMV"] || [extension isEqualToString:@"MOV"])
        {
            return @"Video";
        }
        else if([extension isEqualToString:@"ANS"] || [extension isEqualToString:@"ASC"] || [extension isEqualToString:@"CCF"] || [extension isEqualToString:@"DAT"] || [extension isEqualToString:@"EGT"] || [extension isEqualToString:@"FDX"] || [extension isEqualToString:@"FTM"] || [extension isEqualToString:@"LWP"] || [extension isEqualToString:@"NB"] || [extension isEqualToString:@"ODM"] || [extension isEqualToString:@"ODT"] || [extension isEqualToString:@"OTT"] || [extension isEqualToString:@"OMM"] || [extension isEqualToString:@"PAP"] || [extension isEqualToString:@"RTF"] || [extension isEqualToString:@"RTFA"] || [extension isEqualToString:@"STW"] || [extension isEqualToString:@"SXW"] || [extension isEqualToString:@"TEX"] || [extension isEqualToString:@"INFO"] || [extension isEqualToString:@"TXT"] || [extension isEqualToString:@"UOF"] || [extension isEqualToString:@"WDP"] || [extension isEqualToString:@"WRF"])
        {
            return @"Text";
        }        else if([extension isEqualToString:@"AIFF"] || [extension isEqualToString:@"AU"] || [extension isEqualToString:@"BWF"] || [extension isEqualToString:@"CAF"] || [extension isEqualToString:@"RAW"] || [extension isEqualToString:@"WAV"] || [extension isEqualToString:@"LA"] || [extension isEqualToString:@"PAC"] || [extension isEqualToString:@"M4A"] || [extension isEqualToString:@"TTA"] || [extension isEqualToString:@"WV"] || [extension isEqualToString:@"WMA"] || [extension isEqualToString:@"BRSTM"] || [extension isEqualToString:@"AST"] || [extension isEqualToString:@"MP2"] || [extension isEqualToString:@"MP3"] || [extension isEqualToString:@"WMA"] || [extension isEqualToString:@"AAC"] || [extension isEqualToString:@"M4P"] || [extension isEqualToString:@"RA"] || [extension isEqualToString:@"RM"] || [extension isEqualToString:@"OTS"] || [extension isEqualToString:@"SWA"] || [extension isEqualToString:@"BAND"] || [extension isEqualToString:@"MID"] || [extension isEqualToString:@"MIDI"])
            
        {
            return @"Audio";
        }
        else if ([extension isEqualToString:@"ANI"] || [extension isEqualToString:@"ART"] || [extension isEqualToString:@"BEF"] || [extension isEqualToString:@"BMP"] || [extension isEqualToString:@"CAL"] || [extension isEqualToString:@"CIN"] || [extension isEqualToString:@"CPC"] || [extension isEqualToString:@"CPT"] || [extension isEqualToString:@"DPX"] || [extension isEqualToString:@"ECW"] || [extension isEqualToString:@"EXR"] || [extension isEqualToString:@"FITS"] || [extension isEqualToString:@"FLIC"] || [extension isEqualToString:@"FPX"] || [extension isEqualToString:@"GIF"] || [extension isEqualToString:@"HDRi"] || [extension isEqualToString:@"ICER"] || [extension isEqualToString:@"ICNS"] || [extension isEqualToString:@"ICO"]|| [extension isEqualToString:@"CUR"] || [extension isEqualToString:@"ICS"] || [extension isEqualToString:@"ILBM"] || [extension isEqualToString:@"JBIG"] || [extension isEqualToString:@"JBIG2"] || [extension isEqualToString:@"JNG"] || [extension isEqualToString:@"JPG"] || [extension isEqualToString:@"JPEG"] || [extension isEqualToString:@"MNG"] || [extension isEqualToString:@"MIFF"] || [extension isEqualToString:@"PBM"] || [extension isEqualToString:@"PCX"] || [extension isEqualToString:@"PGF"]|| [extension isEqualToString:@"PGM"] || [extension isEqualToString:@"PICtor"] || [extension isEqualToString:@"PNG"] || [extension isEqualToString:@"PPM"] || [extension isEqualToString:@"PSD"] ||[extension isEqualToString:@"FLIC"] || [extension isEqualToString:@"FPX"] || [extension isEqualToString:@"GIF"] || [extension isEqualToString:@"HDRi"] || [extension isEqualToString:@"ICER"] || [extension isEqualToString:@"ICNS"] || [extension isEqualToString:@"PSB"]|| [extension isEqualToString:@"PSP"] || [extension isEqualToString:@"QTVR"] || [extension isEqualToString:@"RAD"] || [extension isEqualToString:@"RGBE"] || [extension isEqualToString:@"SGI"] || [extension isEqualToString:@"SVG"] ||[extension isEqualToString:@"TGA"] || [extension isEqualToString:@"FPX"] || [extension isEqualToString:@"GIF"] || [extension isEqualToString:@"HDRi"] || [extension isEqualToString:@"ICER"] || [extension isEqualToString:@"ICNS"] ||[extension isEqualToString:@"ICO"]|| [extension isEqualToString:@"TIFF"] || [extension isEqualToString:@"WBMP"] || [extension isEqualToString:@"WebP"] || [extension isEqualToString:@"XBM"] || [extension isEqualToString:@"XCF"] || [extension isEqualToString:@"XPM"])
        {
            return @"Image";
        }else if ([extension isEqualToString:@"XML"])
        {
            return @"Xml";
        }
    }
    
    return @"Unknown";
    
}


+ (NSString *)getFileExtension:(NSString *)file
{
    if (file)
    {
        NSArray *tmpArray = [file componentsSeparatedByString:@"."];
        if (tmpArray.count > 1)
            return tmpArray.lastObject;
    }
    return nil;
}

+ (NSString *)getFileNameFromFullPath:(NSString *)filepath
{
    NSString *tmpFilePath = [NSString stringWithFormat:@"%@", filepath];
    NSArray *tmpArray = [tmpFilePath componentsSeparatedByString:@"/"];
    return [tmpArray lastObject];
}

+ (NSString *)stringWithoutSubStringAtTheFront:(NSString *)originalString andSubString:(NSString *)subString andCaseSensitive:(BOOL)sensitive
{
    if (originalString.length <= subString.length - 1) return originalString;
    
    NSString *tmpOriginalString = [originalString copy];
    NSString *tmpSubString = subString;
    NSRange tmpRange;
    tmpRange.length = subString.length;
    tmpRange.location = 0;
    
    if (sensitive)
    {
        if ([[tmpOriginalString substringWithRange:tmpRange] isEqualToString:tmpSubString])
            tmpOriginalString = [[tmpOriginalString substringFromIndex:tmpRange.length] copy];
    }else
    {
        NSString *tmpOriginalString_LowerCase = [tmpOriginalString lowercaseString];
        tmpSubString = [tmpSubString lowercaseString];
        if ([[tmpOriginalString_LowerCase substringWithRange:tmpRange] isEqualToString:tmpSubString])
            tmpOriginalString = [[tmpOriginalString substringFromIndex:tmpRange.length] copy];
    }
    
    return tmpOriginalString;
}


+ (NSString *)getFileNameWithoutExtension:(NSString *)filename
{
    NSString *tmpFileName = [filename copy];
    NSArray *tmpArray = [tmpFileName componentsSeparatedByString:@"."];
    
    if (tmpArray.count > 1)
    {
        NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] initWithArray:tmpArray];
        [tmpMutableArray removeLastObject];
        NSString *tmpStr_Result = [[tmpMutableArray componentsJoinedByString:@"."] copy];
        return tmpStr_Result;
    }
    
    return filename;
}

+ (NSArray *)getAllFilesUnderDirectory:(NSString *)directory andSubDirectory:(BOOL)subDirectory
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error = nil;
    NSMutableArray *tmpMutableArray = [[NSMutableArray alloc] init];
    [tmpMutableArray removeAllObjects];
    
    NSArray *tmpArray_AllFileNames = [fm contentsOfDirectoryAtPath:directory error:&error];
    
    if (error)
    {
        NSLog(@"Error: %@", error);
        return nil;
    }else
    {
        for (NSString *tmpFileName in tmpArray_AllFileNames)
        {
            NSString *tmpFileFullPath = [NSString stringWithFormat:@"%@/%@", directory, tmpFileName];
            BOOL bIsDirectory = NO;
            if ([fm fileExistsAtPath:tmpFileFullPath isDirectory:&bIsDirectory])
            {
                if (!bIsDirectory)
                    [tmpMutableArray addObject:tmpFileFullPath];
                else
                {
                    if (subDirectory)
                    {
                        NSArray *tmpArray_AllFilesForSubDirectory = [PHFECommon getAllFilesUnderDirectory:tmpFileFullPath andSubDirectory:subDirectory];
                        [tmpMutableArray addObjectsFromArray:tmpArray_AllFilesForSubDirectory];
                    }
                }
            }
        }
    }
    
    return tmpMutableArray;
}

@end