//
//  AppDelegate.h
//  PHFileExplorer
//
//  Created by Ricol Wang on 7/08/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
