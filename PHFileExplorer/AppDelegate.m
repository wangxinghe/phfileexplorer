//
//  AppDelegate.m
//  PHFileExplorer
//
//  Created by Ricol Wang on 7/08/2014.
//  Copyright (c) 2014 Philology. All rights reserved.
//

#import "AppDelegate.h"
#import "FileExplorerViewController.h"
#import <PHCommon/utils.h>
#import "CacheCentre.h"

@interface AppDelegate () <PHFileExplorerDelegate>

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    NSLog(@"Document: %@", DOCUMENTS_DIRECTORY);
    
    FileExplorerViewController *tmpVC = [[FileExplorerViewController alloc] initWithPath:DOCUMENTS_DIRECTORY andRoot:@"/Documents" andRowForIpadLandscape:-1 andRowForIPadPortrait:-1 andRowForIPhoneLandscape:-1 andRowForIPhonePortrait:-1];
    tmpVC.delegate = self;
    tmpVC.bPDFPreview = YES;
    tmpVC.bImagePreview = YES;
//    [[CacheCentre sharedInstance] saveToDisk:YES];
//    tmpVC.bAutoExtract = YES;
//    tmpVC.bHideCompressedFile = YES;
    UINavigationController *tmpNav = [[UINavigationController alloc] initWithRootViewController:tmpVC];
    
    self.window.rootViewController = tmpNav;
    
    [self.window makeKeyAndVisible];
    return YES;
}

@end
